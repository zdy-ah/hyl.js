//固定字典表、动态字典表、表单记录表、中间流水表、当前状态表、汇总分析表
var tablesconfig1=[
	
//字典分类表
//格式:就一个字段_data 存放数组数据
{	tablename:"tb_types",
	fields:[	
		["_data","","","json文本"]
	],
	type:'dict',
	keyid:'Z' //自定义
},


//登录校验
{
	tablename:"tb_users",
	fields:[
		["ulogin","","","昵称"],	
		["upass","","","密码"],
		["autho","","","授权"],		
		["url","","","第三方登录"]	,		
		["token","","","token"]	,
		["days","n","","保留天数"]	,
		["tel","","","手机号"],
		["status","n","1","状态","1 有效，0作废"]
	],
	keyid:'G' //guid
},
//重要资料表
{
	tablename:"tb_users1",
	fields:[
		["uid","","","用户编号"],	
		["uname","","","昵称"],	
		["sex","","","性别"],
		["motto","","","座右铭"],
		["uimg","","","头像"],		
		["uqq","","","QQ"],
		["weixin","","","微信号"],
		["addr","","","地址"],
		["zipc","","","邮编"],
		["email","","","Email"],	
		["remark","","","备注"]			
	],
	keyid:'G'
},
//联系人表
{
	tablename:"tb_contacts",
	fields:[
		["cid","","","联系人编号"],	
		["pysx","","","拼音缩写"],		
		["djrq","","","登记日期"],		
		["cgroup","","","分组"],
		["cclass","","","标签"],
		["clevel","","","标记等级"]	,
		["uname","","","昵称"],	
		["tel","","","手机号"],
		["uqq","","","QQ"],
		["weixin","","","微信号"],
		["addr","","","地址"],
		["email","","","Email"],
		["remark","","","备注"]	,
		["zipc","","","邮编"],
		["uimg","","","头像"],
		["sex","","","性别"],		
		["status","n","1","状态","1 有效，0作废"]
	],
	keyid:'G'

},
//物品表 
//价格表 价格类型：进价，售价
//格式 levelprices:{类型:价格,类型:价格}
{
	tablename:"tb_goods",
	fields:[		
		["gid","","","物品编号","条码或自定义码"],			
		["gname","","","物品名称"],
		["gclass","","","物品分类"],
		["pysx","","","拼音缩写"],		
		["spec","","","单位规格型号"],
		["weight","n","","单位重量"],
		["cbcm","n","","单位体积"],
		["gaddr","","","产地"],
		["price1","n","","最近一次进价"],
		["price2","n","","最近一次售价"],		
		["levelprices","arr","","各级价格"],	
		["gnote","","","说明"],
		["gimg","","","图片"],
		["quality","n","","保质期"],
		["status","n","1","状态","1 记录库存，2 不记库存，0作废"]
	],
	keyid:'G'
	
},


//采购|销售|红冲单，状态：开启，确认，关闭 以最后一次下单时间为准
{
	tablename:"tb_io",
	fields:[	
		["pid","","","表单编号"],		
		["cid","","","供应商|客户编号"],	
		["pors","n","","交易方式","业务流水中的流程代码 b1 采购，s1销售  Dict_tradeType "],	
		["amount","n","","金额"],	
		["tpnum","n","","品种数"],		
		["allnum","n","","总数"],
		["allweight","n","","总重量"],
		["allsize","n","","总重量"],	
		["sdate","","","下单日期"],
		["edate","","","结单日期"],
		["remark","","","备注"],
		["status","n","1","状态","0 作废，1 草稿，2)制定计划:出库/入库,3)审核后发货/签收 转财务记账,4)归档|转入历史表,注：付款流程不在此流程中"]
	],
	keyid:'G' //guid
},
//关于批次问题 在本系统中不处理
//采购|销售|退货单 明细，
//明细关闭后不能改，确认后数量不可以调整，物品编号不可重复
{
	tablename:"tb_io1",
	fields:[	
		["did","","","流水编号","自增"],
		["cid","","","供应商|客户编号"],	
		["pid","","","表单编号"],			
		["gid","","","物品编号"],	
		["price","n","","交易价格"],		
		["num","n","","数量"],	
		["pors","n","","交易方式","业务流水中的流程代码 b1 采购，s1销售  Dict_tradeType "],	
		["damount","n","","总金额"],					
		["weight","n","","总重"],	
		["size","n","","总体积"],	
		["remark","","","备注"],
		["lacknum","n","","缺货数量",'销售提单时如果库存不足需要做缺货登记'],	
		["ldate","","","缺货日期"],
		["status","n","1","状态","1 有效，0作废"]
	],
	keyid:'G' //guid
},


//盘点和红冲不是一个概念,红冲是对A操作的反向操作,盘点是对数量非系统改变的检测记录
//盘点损益单 明细，明细关闭后不能改，确认后数量不可以调整
{
	tablename:"tb_checknum",
	fields:[	
		["did","","","流水编号","自增"],					
		["gid","","","物品编号"],		
		["oldnum","n","","原数量"],	
		["num","n","","实际数量"],	
		["dvalue","n","","差值","自动计算"],	
		["price","","","成本价","损失的单价以进价为准，溢出的单价以上一次进价为准"],
		["sdate","","","日期"],		
		["remark","","","损益说明"],		
	],
	keyid:'G' //guid
},

//出入库流水表  tb_io2改为tb_store_io
{
	tablename:"tb_store_io",
	fields:[	
		["pors","n","","交易方式","业务流水中的流程代码 b1 采购，s1销售  。。。 "],	
		["gid","","","物品编号"],		
		["price1","n","","进价"],	
		["price2","n","","售价"],	
		["num","n","","数量"],	
		["sdate","","","日期"],
		["remark","","","备注"]
	],
	keyid:'G' //guid
},

//当前库存状态表
{
	tablename:"tb_store",
	fields:[	
		["gid","","","物品编号"],		
		["price1","n","","进价"],	
		["num","n","","当前数量"],	
		["sdate","","","日期"],
	],
	keyid:'G' //guid
},
//每个月与供应商和代理商交易的特定商品的买入或卖出的最价格近一次价格，数量
//往来商品统计表
{
 	tablename:"tb_price_mon",
	fields:[	
		["monnum","n","","记账月份"],		
		["cid","","","交易对象"],
		["gid","","","商品"],
		["num","n","","月交易数量","???"],
		["price","n","","交易价格"],
		["pricel","n","","交易最低价格"],
		["priceh","n","","交易最高价格"]	
	],
	keyid:'G' //guid
},
//库存统计表,整个系统是普及版,不涉及多批号管理,要批号管理功能需要定制开发,或者自定义为新产品
{
	tablename:"tb_store_mon",
	fields:[			
		["monnum","","","月"],
		["gid","","","物品编号"],		
		["initnum","n","","月初数量"],		
		["num","n","","当前数量"],					
		["rsnum","n","","月退货数量","客户退货数量"],	
		["rbnum","n","","月退货数量","退货给供应商的数量"],	
		["bnum","n","","月进货数量",""],	
		["snum","n","","月出货数量",""],
		["synum","n","","月损溢数量",""],
		["samout","n","","月销售金额","销售时增加退货时减少"],
		["camout","n","","月采购金额","销售时增加退货时减少"],
		["profit","n","","月利润",""],
		["lose","n","","月盘点损溢",""],
	],
	keyid:'G' //guid
},


//往来收支流水  状态：都是有效的
{
	tablename:"tb_journal",
	fields:[	
		["jid","","","凭证编号"],
		["cid","","","交易对象"],
		["mode","","","交易方式","相当于交易科目，财务凭证中的流程代码，决定了相关的交易账户"],
		["amount","n","","交易额","正,不能为负数"],
		["receivable","n","","应收款","改变量"],
		["payable","n","","应付款","改变量"],
		["capital","n","","存款","钱，存款，改变量"],		
		["edate","","","交易日期"],
		["remark","","","说明",""],
		["monnum","","","记账月份"]
	],
	keyid:'G' //guid
},

//个人月账本（统计表）
{
 	tablename:"tb_myacct",
	fields:[	
		["monnum","","","记账月份"],
		["receivable","n","","应收款","总的销售应收，累计"],
		["payable","n","","应付款","总的采购应付,累计"],
		["capital","n","","存款","钱，存款,累计"],	
		["camout","n","","采购额","采购价值，月累计"],	
		["samout","n","","销售额","销售价值，月累计"],	
		["income","n","","收入","实际收入，存款,累计增加的部分，每个月清零"],	
		["expend","n","","支出","实际支出，存款,累计减少的部分，每个月清零"],	
		["lose","n","","盘盈亏","盘点引发的 损失或者溢出，每个月清零"],	
		["profit","n","","订单利润合计","合计利润=订单利润之和，但！=(income-expend)（仅对销售单和销售退货单有效），每个月清零"],	
	],
	keyid:'G' //guid
},
//往来月账单（每月为每个供应商和客户设定一个账单）（统计表）
{
 	tablename:"tb_tradeacct",
	fields:[	
		["monnum","n","","记账月份"],		
		["cid","","","交易对象"],
		["receivable","n","","应收款"],
		["payable","n","","应付款"],
		["income","n","","收入","钱，存款,累计增加的部分，每个月清零"],	
		["expend","n","","支出","钱，存款,累计减少的部分，每个月清零"],
		["profit","n","","订单利润合计","当月累计订单利润之和（仅对销售单和销售退货单有效），每个月清零"],	
		["samout","n","","销售额","销售之和，（仅对销售单和销售退货单有效）"],
		["camout","n","","采购额","采购之和，（仅对采购单和采购退货单有效）"],	
		//月成本=月收入-月利润
	],
	keyid:'G' //guid
},

//财务 收款|付款单  状态：付款后核减采购单，收款后核减销售单
//不要试图把收付款和订单结算同时进行，现实中因为收付款等问题
//tb_receipt  tb_settle
{
	tablename:"tb_settle",
	fields:[	
		["reid","","","表单编号"],		
		["cid","","","交易对象"],
		["mode","","","方式","Dict_acctType"],		
		["amount","n","","合计应付|应收","合并后的订单应付费用"],			
		["reduces","n","","实际核减","核减"],	
		["discount","n","","优惠金额","扣除金额，自动做红冲处理"],			
		["paid","n","","实际付款","实际付款的费用=实际核减-优惠金额"],		
		["paytype","","","支付方式"],
		["sdate","","","核减日期"],
		["remark","","","说明",""],
		["status","n","1","状态","1保存，2提交，没有0"]
	],
	keyid:'G' //guid
}

]

var	账户={
		eff:{"存款":1 ,"应付款":2 ,"应收款":3 ,"货款":4}
}

function modelbyconfig(config) {
	for(var table in config) {
		var fields = {}
		key = config[table].tablename;
		fds = config[table].fields;
		for(var i = 0; i < fds.length; i++) {
			fields[fds[i][0]] = ''
		}
		localStorage.setItem(key, JSON.stringify(obj))
	}	
}

var getConfig = function(config) {
	var cc = config;
	var tablenames = [];
	for (var i=0;i<cc.length;i++ ) {
		var tablename = cc[i].tablename;
		//if (tablename == undefined || tablename == null) continue;
		tablenames.push(tablename);
	}
	//console.log("tableNames", tablenames)
	return {
		"tableNames": tablenames,
		"getTable": function(tablename) {
			for (var i = 0;i < cc.length; i++) {
				if (cc[i].tablename == tablename) {
					return cc[i];
				}
			}
			return null;
		}
	};
}(tablesconfig1);

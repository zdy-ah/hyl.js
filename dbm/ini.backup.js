/**
 * 文件数据库管理器
 */
(function(e,t) {
		if ("object" == typeof exports && "object" == typeof module) module.exports._Backup = new t();
		else if ("function" == typeof define && define.amd) define(t);	
		else e._Backup = new t(); //对象
})(this,function () {
		/**数据转文件
		 * @param {Object} path
		 * @param {Object} successCB
		 */
		this.db2file = function(path, successCB) {

			var file = path + "db_" + Date2Number() + '';
			var content = {
				tables: [],
				datas: []
			}
			for (var i = 0; i < tablesarr.length; i++) {
				var tablename = tablesarr[i];
				content.tables.push(tablename);
				var tab1 = new XCDBHelp(tablename);
				var rs = tab1.getList();
				content.datas.push(rs);
				//console.log(JSON.stringify(rs));
			}
			var c = JSON.stringify(content);

			_FI.writeFile(file, c, {
					method: 0
				},
				function(target) {
					var src = target.toURL();
					_FI.zip(src, src,
						function() {
							target.remove();
							console.log("备份完成！")
							if (successCB) {
								successCB(src, src + ".zip");
							}

						},
						function(error) {
							console.log("Compress error!" + error.message);
						});
				},
				function(e) {
					console.log(e.message);
				});
		}
		/**
		 * 文件转数据
		 * @param {Object} zipfile
		 */
		this.file2db = function(zipfile) {
			var targetFile = zipfile.slice(0, zipfile.lastIndexOf("."));
			var w1 = _UI.wait("　　 开始还原...　　");
			var zf = _FI.analyseUrl(targetFile);
			_FI.unzip(zipfile, zf.path,
				function() {
					//console.log("cccd"+targetFile);
					_FI.readFile(targetFile, function(txt) {
						//console.log("txt="+txt);
						//return;
						var c = JSON.parse(txt);
						//var mask = mui.createMask();
						//mask.show()				
						for (var i = 0; i < c.tables.length; i++) {
							var tablename = c.tables[i];
							var tab1 = new XCDBHelp(tablename);
							var arrdata = c.datas[i];
							tab1.clear();
							tab1.setList(arrdata);
							//console.log(tablename+':'+JSON.stringify(arrdata)+'\n')
						}
						//console.log("txt="+zf.path);
						_FI.deleteDirByURL(zf.path, function(e) {
							w1.setTitle("数据还原完毕！");
							setTimeout(function() {
								w1.close()
							}, 1000)

						})
						//mask.close()

					}, function(e) {
						console.log("文件读取异常 " + e.message);
					});

				},
				function(e) {
					console.log("zipfile URL failed: " + e.message);
				});

		}
		/**
		 * 获取图片列表
		 */
		this.getImgList = function() {
			//读取数据库的产品图片生成新的json
			var _tb_goods = new XCDBHelp('tb_goods')
			var list = _tb_goods.getList();
			var uplist = [];
			for (var i = 0; i < list.length; i++) {

				var g = list[i].gimg;
				for (var j = 0; j < g.length; g++) {
					var img = {
						name: '',
						stamp: '',
						fsize: ''
					};
					img.name = g[j].name;
					img.stamp = g[j].stamp;
					img.fsize = g[j].size;
					uplist.push(img);
				}
			}
			return uplist;

		}
		this.saveimg = function(filename) {


		}
	}
);

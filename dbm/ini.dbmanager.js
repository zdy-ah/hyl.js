/**
 * sqlite数据库管理器
 */
(function(e, t) {
	if ("object" == typeof exports && "object" == typeof module) module.exports.DBManager = t;
	else if ("function" == typeof define && define.amd) define(t);
	else e.DBManager = t; //需要实例化
})(this, function(config) {
	this.config = config;
	//sql语句执行失败后执行的回调函数  
	function onError(tx, error) {
		alert("操作失败，失败信息：" + error.message);
	}

	this.truncateTable = function() {
		var sql = "truncate table " + _tb.tablename;
		return sql;
	}
	//【字段0】【字段别名1】【类型2】【主键类型3】【不为空4】【默认值5】【字段说明6】
	//【自动类型】"" :非主键, pt: 普通主键，zz：自增主键，uu：uuid主键，sj:时间戳
	//【不为空】0|"" :可以为空,1:不可为空

	this.resetAll = function() {
		var arr = _Fun.a.concat(_DBM.dropAll(), _DBM.createAll(), _DBM.indexAll());
		//console.log("resetAll",arr)
		return arr;
	}
	this.createAll = function() {
		var tarr = [];
		for (var i in this.config) {
			table = this.config[i];
			var ta = this.createTable(table);
			tarr.push(ta);
		}
		return tarr;
	};

	this.dropAll = function() {
		var tarr;
		for (var i in this.config) {
			table = this.config[i];
			var arr = this.dropTable(table);
			tarr = _Fun.a.concat(tarr, arr);
		}
		return tarr;
	};
	this.insertAll = function() {
		var tarr = [];
				for (var i in this.config) {
			table = this.config[i];
			var ta = this.insertRecord(table);
			if (ta)
				tarr.push(ta);
		}
		return tarr;
	};
	this.indexAll = function() {
		var tarr = [];

		for (var i in this.config) {
			table = this.config[i];
			var ta = this.indexTable(table);
			if (ta)
				tarr.push(ta);
		}

		return tarr;
	};

	this.modelAll = function(table) {
		var tarr = [];   
				for (var i in this.config) {
			table = this.config[i];
			var ta = this.createModel(table);
			if (ta)
				tarr.push(ta);
		}
	  return "{" +tarr.join(",\r\n")+"};";
	}
	this.dropTable = function(table) {
		var tarr = [];
		if (table.index)
			tarr.push("\r\ndrop index if exists idx_" + table.tablename + ";");
		tarr.push('\r\ndrop table if exists ' + table.tablename + "; ");
		return tarr;
	}

	this.createModel = function(table,obj) {
		var arr = [];
		arr.push(''+table.tablename + ':{\r\n ');
		arr.push('idkey:"",\r\n'); //目前编号
		arr.push('datekey:"",\r\n'); //时间分区格式 yyyymm,m3,m2,m12,m13,yyyymmdd,d7,d31
		arr.push('auto:"",\r\n'); //主键类型  ‘zz’： 自增，‘pt’： 普通，‘uu’： uuid型 ‘sj’时间戳
		arr.push('chgdate:""\r\n'); //更新日期	
		arr.push("}");
		return arr.join("");
	}
	this.selectTable = function(table) {
		return "\r\nselect * from " + table.tablename + ";";
	}
	this.indexTable = function(table) {
		if (table.index)
			return "\r\n create index  IF NOT EXISTS  idx_" + table.tablename + " on " + table.tablename + "(" + table.index +
				");";
		else
			return null;
	}
	/**
	 * @param {Object} table
	 * @param {Object} obj 不为空时，只创建obj相关的字段
	 */
	this.createTable = function(table, obj) {
		if (table.tablename == undefined) console.log("table", table)
		var mode = " \r\n %%field0 %%type1 %%default %%notnull %%auto "; // %%fnote
		var carr = [];
		carr.push("\r\ncreate table  IF NOT EXISTS  " + table.tablename + "(");
		var s = _FD.mergeFields(table, mode, ",", obj);
		carr.push(s);
		if (s.indexOf("primary") < 0 && table.key !== undefined)
			carr.push("\r\n , primary key (" + table.key + ")");
		carr.push(");");
		return carr.join(" ");
	};
	/**
	 * @param {Object} table
	 * @param {Object} obj 不为空时，只插入与obj相关的字段
	 */
	this.insertRecord = function(table, obj) {
		var mode1 = " %%field0 ";
		var mode2 = " %%value ";
		var iarr = [];
		iarr.push("insert into " + table.tablename);
		iarr.push("(");
		iarr.push(_FD.mergeFields(table, mode1, ",", obj));
		iarr.push(") values (");
		iarr.push(_FD.mergeFields(table, mode2, ",", obj));
		iarr.push(")");
		var s = iarr.join("");
		return s;
	}
	/**
	 * @param {Object} table
	 * @param {Object} arr arr[0] set 对象表达式 ，arr[1] where 字符串表达式
	 */
	this.updateRecord = function(table, arr) {
		if (_IS.isEmpty(arr)) return;
		var mode1 = " %%field0=%%value  ";
		var iarr = [];
		iarr.push("update " + table.tablename + " set");
		iarr.push(_FD.mergeFields(table, mode1, ",", arr[0]));
		if (!_IS.isEmpty(arr[1]))
			iarr.push(arr[1]);
		var s = iarr.join("");
		return s;
	}
});

//servermapping
var _IOM = new IOMapping();

function IOMapping() {
	var _monnum = new Date().Format('yyyyMM');
	var _lastmon = addmonnum(_monnum, -1);

	//入库出库  type [in ,out]
	//事务操作,库存变化引起财务的变化	
	//md 是主订单的资料
	var tstore = new XCDBHelp('tb_store');
	var tstore_io = new XCDBHelp('tb_store_io');
	var tstore_mon = new XCDBHelp('tb_store_mon');
	var ttradeacct = new XCDBHelp('tb_tradeacct');
	var tmyacct = new XCDBHelp('tb_myacct');
	var tgood = new XCDBHelp('tb_goods');
	var tpricemon = new XCDBHelp('tb_price_mon');
	var tjournal = new XCDBHelp('tb_journal');
	var tio = new XCDBHelp('tb_io');
	var tio1 = new XCDBHelp('tb_io1');
	var tchecknum = new XCDBHelp('tb_checknum');
	//得到物品的库存
	this.getStoreNum = function(gid) {
		var where = {
			"monnum": _monnum,
			"gid": gid
		};
		//console.log(JSON.stringify(where))
		var list = tstore_mon.where(where).getList();
		//console.log(JSON.stringify(list))
		var num = 0;
		if(list.length > 0) {
			num = list[0].num;
		}
		return num;
	}
	//得到当月库存
	this.getCurmonStore = function() {
		var where = {
			"monnum": _monnum
		};
		return tstore_mon.where(where).getList();
	}
	//查询客户当月应付和应收账款
	this.getCurmonDue = function(cid) {

		var where = {
			"monnum": _monnum,
			"cid": cid
		};
		return ttradeacct.where(where).getList();
	}
	//得到商品价格
	this.getGoodPrice = function(gid, type) {

		var obj = tgood.getObjByID(gid);
		if(obj) {
			if(type = '1') //进价
				return obj.price1;
			else
				return obj.price2;
		} else 
		return 0;
	}

	//支持无库存销售，销售时记正利润，采购时记负利润
	this.iostock = function(main, detail) {
		//console.log('this.iostock'+'begin')
		if(main.edate)
			_monnum = main.edate.slice(0, 4) + main.edate.slice(5, 7);
		var m2 = {} //流水账 预备模型
		m2.mode = main.pors;
		var ll = 0 //求订单利润
		for(var i = 0; i < detail.length; i++) {
			var md = detail[i];
			console.log("io1 明细：" + JSON.stringify(md));
			var retv = this.reduceStore(md, main.edate); //合减库存，计算各项商品利润
			md["profit"] = retv.ll;
			ll += retv.ll;			
			//出入库行为 引发 库存变化,更新总库存
			this.updateStoreMon(md);
			//console.log("5555")
			//以交易方+商品维度的交易月状态：记录价格、数量、最高、最低
			if(m2.mode == 'b1' || m2.mode == 's1' || m2.mode == 'rs1' || m2.mode == 'rb1')
				updatetbprice_mon(md);
		}
		//修改原单据利润与凭证一致	
		//main.profit=ll;	
		//财务变化		
		m2.cid = main.cid;
		m2.amount = main.amount;
		m2.edate = main.edate;
		m2.profit = ll;
		//console.log('m2.profit='+ JSON.stringify(m2));
		this.addJournal(m2);

		//_tb_io.setObj(main);		
		//console.log('this.iostock'+'end')
	}
	
	//price_mon保存与联系某商品的最近交易价格
	function updatetbprice_mon(md) {
		var data = clone(MYMODEL['tb_price_mon']);

		var qs = tpricemon.where({
			"monnum": _monnum,
			"cid": md.cid,
			"gid": md.gid
		}).getList();

		if(qs.length == 0) { //如果当月库存记录不存在 新插入一条
			data.monnum = _monnum;
			data.cid = md.cid;
			data.gid = md.gid;
		} else { //如果当月最新交易价格数据存在,那么更新交易价格
			data = qs[0];
		}
		//记录客户或供应商的交易数量 方便统计
		if(md.pors == 'rs1' || md.pors == 'rb1'){
			data.num -= md.num;
		}else if(md.pors == 's1' || md.pors == 'b1'){
			data.num += md.num;
		}
		
		data['price'] = md.price;
		data['pricel'] = data['pricel']| md.price;	
		data['priceh'] = data['priceh']|md.price;
		if(data['pricel'] > md.price) {//最低价格
			data['pricel'] = md.price;
		}else if(data['priceh'] < md.price) {//最高价格
			data['priceh'] = md.price;
		}
		console.log('tb_price_mon ' + JSON.stringify(data))
		tpricemon.setObj(data);
	}
	//更新库存
	//	入参数据格式:{
	//		_monnum;
	//		md.gid;
	//		md.num
	//	}
	//更新库存
	// md.pors,gid,num,profit,lose
	this.updateStoreMon = function(md) {

		//console.log('updateStoreMon qs '+JSON.stringify(md))
		if(md == null) return;
		var mode = md.pors;
		//当月库存
		var qs = tstore_mon.where({
			"monnum": _monnum,
			"gid": md.gid
		}).getList();
		//上个月库存
		var qs1 = tstore_mon.where({
			"monnum": _lastmon,
			"gid": md.gid
		}).getList();

		//返回上个月的库存	
		var data = clone(MYMODEL['tb_store_mon']);
		if(qs.length == 0) { //如果当月库存记录不存在 新插入一条
			data._idkey = null;
			data.monnum = _monnum;
			data.samout =0;
			data.gid = md.gid;
			data.initnum = 0;//月初库存
			if(qs1.length > 0)
				data.initnum = qs1[0].num; //月初数量		
			data.num = data.initnum;//当前库存
		} else { //如果当月库存数据存在,那么更新库存
			data = qs[0];
			//console.log('updateStoreMon1 ' + JSON.stringify(data))			
		}
	
		var num = md.num;
		var lose = md.lose;
		if(mode == 's1' || mode == 'rb1' || mode == 'cl1') {
			num = 0 - md.num; //退供应商 减库存				
		}
		if(mode == 'cl1') { //损失的话 lose 为负值
			lose = 0 - md.lose;
		}
		data.num = data.num + num;
		if(mode == "rs1") {
			data.rsnum += md.num;
			data.samout -= md.damount;
		} else if(mode == "s1") {
			data.snum += md.num;
			data.samout += md.damount;
		} else if(mode == "b1") {
			data.bnum += md.num;
			data.camout += md.damount;
		} else if(mode == "rb1") {
			data.rbnum += md.num;
			data.camout -= md.damount;
		} else if(mode == "cl1" || mode == "co1") {
			data.synum += num;
		}
		if(md.profit)
			data.profit += md.profit;
		if(md.lose) {
			data.lose += lose;
			data.profit += md.profit;
		}
		data.profit = round2(data.profit, 4)
		//console.log('updateStoreMon2 ' + JSON.stringify(data))
		tstore_mon.setObj(data);
		//console.log('updateStoreMon'+'end')
	}
//出入库，同时计算利润
	//快递费，红包，包装袋，赠品这些库存中没有商品：1)没有入库就不会减少
	//md.price,gid,num >0 ,pors
	this.reduceStore = function(md, sdate) {
		var mode = md.pors;
		var data = clone(MYMODEL['tb_store']);
		data.gid = md.gid;
		var price = 0;
		data.price1 = price = round2(md.price, 3);
		data.sdate = sdate;
		var num = md.num; //销售数量或采购数量 num必须是绝对值 大于0
		if(!num) return null;
		var ll = 0; //商品利润
		var qs1 = tstore.where({
			"gid": md.gid
		}).getList();
		var arr = _AW.sort(qs1, "sdate");
		//console.log('tstore,mode:' + mode)
		//console.log('tstore,arr:' + JSON.stringify(arr))
		if(mode == 's1' || mode == 'rb1') { 
			//销售给客户或退货给供应商时，减库存
			//如果库存充足 可以正常减库存计算利润
			//如果库存不足，需要做负库存，直接以采购价=0的计算利润，采购入库时再减利润
			for(var i = 0; i < arr.length; i++) {
				var o = arr[i];
				if(o.num <= 0) break; //库存已经不足，无法核减
				// {"ll":ll,lack:0};
				if(o.num >= num) { //如果库存比销售的数量大，只扣销售数量
					ll += num * (price - o.price1);
					o.num = o.num - num;
					var changenum = num;
					num = 0;
					if(o.num == 0) {
						tstore.delObjById(o._idkey);
					} else {
						tstore.setObj(o); //更新
					}
					newstore_io(mode, md.gid, o.price1, md.price1, changenum, sdate, '出库 减库存');
					//console.log('tstore o.num > num:' + JSON.stringify(o));
					break; // {"ll":ll,lack:0};
				} else { //否则 库存全部减去，继续查询下一条库存
					ll += o.num * (price - o.price1);
					num = num - o.num;
					var changenum = o.num;
					tstore.delObjById(o._idkey);
					newstore_io(mode, md.gid, o.price1, md.price1, changenum, sdate, '出库-减库存');
					//console.log('tstore s1 delObjById：' + JSON.stringify(o));
				}
			}

			//如果不够减，插入库存表，记负库存，并全部做成利润
			if(num > 0) {
				ll += round2(price * num, 3); //利润=(销售价格-0 )*数量
				data.num = round2(0 - num, 4); //做负库存
				tstore.setObj(data); //插入
				//console.log('tstore,num>0:' + JSON.stringify(data));
				var changenum = round2(num, 4);
				newstore_io(mode, data.gid, 0, data.price1, changenum, sdate, '销售出库.记负库存');
				setIo1Lack1(md, num, sdate); //登记缺口	
			}

		} else if(mode == 'b1' || mode == 'rs1') {
			//采购和销售退货时，可以抵消利润
			// num //采购-商品数量
			var lacknum = 0; //待核减数量
			//arr= tb_store.list
			for(var i = 0; i < arr.length; i++) {
				var o = arr[i];
				if(o.num > 0) break; //如果库存为正,说明没有欠数，不用进行任何处理????		
				//假设o.num<0			
				if((o.num + num) <= 0) { //如果采购数量num，不足以弥补亏欠，有多少算多少，先合减并计算负利润
					lacknum += num;
					ll -= round2(num * md.price, 3); //num>0	
					var changenum = num;
					o.num = o.num + num;
					num = 0;
					if(o.num == 0) {
						tstore.delObjById(o._idkey);
					} else {
						tstore.setObj(o);
					}
					newstore_io(mode, o.gid, price, o.price1, changenum, sdate, '入库.抵负库存');
					//console.log('tstore, (o.num+num)<0:' + JSON.stringify(o));
					break; //{"ll":ll,lack:0};
				} else { //如果采购数量大于亏欠数量，直接抵扣亏欠数量，计算负利润
					lacknum -= o.num; //0-o.num>0	
					ll += round2(o.num * md.price, 3); //o.num<0	
					var changenum = 0 - o.num;
					num = o.num + num; //剩余的与下一条库存比较
					newstore_io(mode, o.gid, price, o.price1, changenum, sdate, '入库.抵负库存');
					tstore.delObjById(o._idkey);

					console.log('tstore b1 delObjById：' + JSON.stringify(o));
				}
			}
			if(num > 0) { //多出来的数量插入库存表			
				var changenum = data.num = round2(num, 4);
				newstore_io(mode, data.gid, price, 0, changenum, sdate, '入库+库存');
				tstore.setObj(data); //新增一条

				//console.log('tstore,num>0:' + JSON.stringify(data))
			}
			if(lacknum > 0) {
				setIo1Lack2(md.gid, lacknum, md.price); //通过核减数量*进价，减少利润			
			}
		}
		return {
			"ll": ll
		}; //没有利润 也没有核减
	}
	//["valuegoods","n","","货物价值","货物价值改变量"], 删除
	//充钱提款时记财务凭证
	//记录交易凭证
	this.addJournal = function(md) {
		//console.log('addJournal'+'begin')
		var data = clone(MYMODEL['tb_journal']); // 流水账最终模型
		data.cid = md.cid;
		data.mode = md.mode;//s1，b1。。等
		data.amount = md.amount;//钱
		data.receivable = 0; //+-自己的应收
		data.payable = 0; //+-自己的应付
		data.capital = 0; //+-自己的存款
		data.edate = md.edate;
		//console.log('addJournal md ' + JSON.stringify(md))
		//console.log('addJournal md.edate ' + md.edate)
		if(md.edate)
			data.monnum = md.edate.slice(0, 4) + md.edate.slice(5, 7);
		else
			data.monnum = _monnum;
		mode2acct(data);		
		data.remark = md.remark|'';

		//console.log('tjournal ' + JSON.stringify(data))
		tjournal.setObj(data);
		//记录账本
		data.profit = md.profit; //利润 有正负，来自订单
		//包含盘点
		set_myacct(data)
		//排除盘点
		if(md.mode == 's1' || md.mode == 's2' || md.mode == 'b1' || md.mode == 'b2' || md.mode == 't1' || md.mode == 't2' || md.mode == 'rb1' || md.mode == 'rs1') {
			set_tradeacct(data)
		}
		//console.log('addJournal'+'end')
	}
	//交易模式
	function mode2acct(data) {
		console.log('mode2acct data.mode=' + data.mode)
		var amount = round2(data.amount, 3);
		if(data.mode == 'h1') { //红冲存款 优惠时启用 ，或直接红冲
			data.remark = '红冲存款=' + amount;
			data.capital = amount;
		} else if(data.mode == 't1') { //红冲应收款 客户退货时 ，或直接红冲
			data.remark = '红冲应收款=' + amount;
			data.receivable = amount;
		} else if(data.mode == 't2') { //红冲应付款 退货给供应商时 ，或直接红冲
			data.remark = '红冲应付款=' + amount;
			data.payable = amount;
		} else if(data.mode == 's1') { //卖.发货: 货物价值减少,应收款增		
			data.remark = '销售出库凭证  本单应收=' + amount;
			data.receivable = amount; //正表示 客户的欠款
		} else if(data.mode == 's2') { //收款: 应收款减 ->存款增
			data.remark = '收款单 本次收款=' + amount;
			data.receivable = 0 - amount;
			data.capital = amount;
		} else if(data.mode == 'b1') { //买.签收:存款减 , 应付款增		
			data.remark = '采购进货凭证 本单应付=' + amount;
			data.payable = amount; //正表示 欠供应商款
		} else if(data.mode == 'b2') { //付款:应付款减 , 存货数量增加
			data.remark = '付款单 本次付款=' + amount;
			data.payable = 0 - amount;
			data.capital = 0 - amount;
		} else if(data.mode == 'rs1') { //客户退货。签收:存款减 , 减应收款		
			data.remark = '客户退货-签收凭证 本单应还=' + amount;
			data.receivable = 0 - amount; //
		} else if(data.mode == 'rb1') { //退供应商。发货: 货物价值减少,减应付款
			data.remark = '退供应商-发货凭证  本单应收=' + amount;
			data.payable = 0 - amount; //
		}
		//	console.log('mode2acct'+JSON.stringify(data)+'end')
	}

	//记录个人账本
	//md数据格式:edate,acct,amount
	function set_myacct(md) {
		//console.log('set_myacct'+'begin')
		if(md == null) return;
		var qs = tmyacct.where({
			"monnum": _monnum
		}).getList();
		var qs1 = tmyacct.where({
			"monnum": _lastmon
		}).getList();

		var data = clone(MYMODEL['tb_myacct']);
		data.monnum = _monnum;

		var payable = 0,
			receivable = 0,
			capital = 0,
			profit = 0,
			income = 0,
			expend = 0,
			samout = 0,
			camout = 0;
		//如果当月数据不存在,新增		
		if(qs.length == 0) {
			//取上个月 数据
			if(qs1.length > 0) {
				receivable = qs1[0].receivable
				payable = qs1[0].payable
				capital = qs1[0].capital
				//income = round2(qs1[0].income, 3);//收入只要计算当月累计就可以了 
				//expend = round2(qs1[0].expend, 3);//支出只要计算当月累计就可以了 
				//profit = round2(qs1[0].profit, 3);//利润只要计算当月累计就可以了 
			}
		} else { //如果当月数据存在,那么取本月数据
			data = qs[0];
			receivable = data.receivable
			payable = data.payable
			capital = data.capital
			income = data.income
			expend = data.expend
			profit = data.profit
			samout = data.samout
			camout = data.camout
		}
		receivable += md.receivable;
		payable += md.payable;
		capital += md.capital;
		profit += md.profit;
		if(data.mode == 's1' || data.mode == 'rs1') {
			samout += md.receivable;
		}
		if(data.mode == 'b1' || data.mode == 'rb1') {
			camout += md.payable;
		}
		//在付款和收款的时候，计算实际到账的钱或实际出去的钱
		if(data.mode == 'b2') {
			expend += md.payable
		} else if(data.mode == 's2') {
			income += md.receivable
		}

		data.receivable = round2(receivable, 3);
		data.payable = round2(payable, 3);
		data.capital = round2(capital, 3);
		data.expend = round2(expend, 3); //付款月累计
		data.income = round2(income, 3); //收款月累计
		data.profit = round2(profit, 3); //利润月累计
		data.samout = round2(samout, 3); //销售额月累计
		data.camout = round2(camout, 3); //采购额月累计

		console.log('tmyacct:' + JSON.stringify(data))
		tmyacct.setObj(data);
		//console.log('set_myacct'+'end')		
	}

	//记录往来账
	function set_tradeacct(md) {
		//console.log('set_tradeacct'+'begin')
		if(md == null) return;
		var data = clone(MYMODEL['tb_tradeacct']);
		var qs = ttradeacct.where({
			"monnum": _monnum,
			"cid": md.cid
		}).getList();
		var qs1 = ttradeacct.where({
			"monnum": _lastmon,
			"cid": md.cid
		}).getList();
		//如果当月数据不存在,新增		

		var receivable = 0,
			payable = 0,
			profit = 0,
			income = 0,
			expend = 0,
			samout = 0,
			camout = 0;
		if(qs.length == 0) {
			if(qs1.length > 0) {
				receivable = qs1[0].receivable
				payable = qs1[0].payable
				//profit =  qs1[0].profit; //利润只要计算当月累计就可以了 
			}
			data.cid = md.cid;
			data.monnum = _monnum;
		} else {
			data = qs[0];
			receivable = data.receivable;
			payable = data.payable;
			profit = data.profit;
			income = data.income;
			expend = data.expend;
			samout = data.samout;
			camout = data.camout;
		}
		if(data.mode == 'b2') {
			expend += md.payable
		} else if(data.mode == 's2') {
			income += md.receivable
		}
		data.expend = round2(expend, 3); //付款月累计
		data.income = round2(income, 3); //收款月累计
		data.receivable = round2(receivable + md.receivable, 3);
		data.payable = round2(payable + md.payable, 3);
		//利润月累计		
		if(md.mode == 's1' || md.mode == 'rs1') {
			profit = profit + md.profit
			samout = samout + md.receivable
		}
		data.profit = round2(profit, 3);
		data.samout = round2(samout, 3);
		if(md.mode == 'b1' || md.mode == 'rb1') {
			camout = camout + md.payable
		}
		data.camout = round2(camout, 3);
		console.log('ttradeacct' + JSON.stringify(data))
		ttradeacct.setObj(data);
		//console.log('set_tradeacct'+'end')
	}

	//抵消从某个客户获取的利润
	function reduceProfit(cid, pf) {

		var qs = ttradeacct.where({
			"monnum": _monnum,
			"cid": cid
		}).getList();
		if(qs.length > 0) {
			qs[0].profit -= pf;
			console.log('tb_tradeacct,减少利润：' + JSON.stringify(qs[0]));
			ttradeacct.setObj(qs[0]);
		}
	}
	//盘点数量
	this.checknum = function(md) {

		console.log('checknum,盘点：' + JSON.stringify(md));
		this.reduceStoreLose(md, new Date().Format());
		//出入库行为 引发 库存变化,更新总库存
		this.updateStoreMon(md);
		this.set_myacctlose(md);

	}

	//记盘点帐
	this.set_myacctlose = function(md) {

		if(md == null) return;
		var qs = tmyacct.where({
			"monnum": _monnum
		}).getList();
		var qs1 = tmyacct.where({
			"monnum": _lastmon
		}).getList();

		var data = clone(MYMODEL['tb_myacct']);
		data.monnum = _monnum;

		var payable = 0,
			receivable = 0,
			capital = 0,
			lose = 0;

		//如果当月数据不存在,新增		
		if(qs.length == 0) {
			//取上个月 数据
			if(qs1.length > 0) {
				data.receivable = round2(qs1[0].receivable, 3);
				data.payable = round2(qs1[0].payable, 3);
				data.capital = round2(qs1[0].capital, 3);
				//income = round2(qs1[0].income, 3);//收入只要计算当月累计就可以了 
				//expend = round2(qs1[0].expend, 3);//支出只要计算当月累计就可以了 
				//profit = round2(qs1[0].profit, 3);//利润只要计算当月累计就可以了 				
			}
		} else { //如果当月数据存在,那么取本月数据
			data = qs[0];
			lose = round2(data.lose, 3); //本月损溢数据			
		}
		//在付款和收款的时候，计算实际到账的钱或实际出去的钱
		if(md.pors == 'cl1') {
			lose -= round2(md.lose, 3);
		} else if(md.pors == 'co1') {
			lose += round2(md.lose, 3);
		}
		data.lose = round2(lose, 3); //付款月累计	
		console.log('tmyacct:' + JSON.stringify(data))
		tmyacct.setObj(data);
	}
	
	//函数：销售时的缺口数量要更新销售明细，已待核减
	//参数：明细记录，缺货的数量,登记日期
	function setIo1Lack1(md, lacknum, ldate) {
		md["lacknum"] = lacknum;
		md["ldate"] = ldate;
		console.log('tb_io1,登记缺口：' + JSON.stringify(md));
		tio1.setObj(md);
	}

	//函数：采购时冲减 销售时因为库存不足，遍历销售单，减少与对应客户的利润
	//参数：商品编号，要冲减的总数量,采购价格
	function setIo1Lack2(gid, rednum, price1) {
		//查找销售单的中该商品的、有缺货的销售明细，按照时间排列
		//遍历销售明细，核减销售时缺少的数量
		//var data = clone(MYMODEL['tb_tradeacct']);
		if(rednum == 0) return;

		//遍历所有缺货销售的单据明细
		var qs = tio1.where({
			"lacknum(": 0,
			"gid": gid,
			"pors": "s1"
		}).getList();
		//console.log('setIo1Lack2，qs：'+ JSON.stringify(qs));
		var arr = _AW.sort(qs, "ldate");
		var num = rednum; //抵消数量

		for(var i = 0; i < arr.length; i++) { //遍历商品销售明细
			var item = arr[i];
			var itemnum = 0; //缺货数量			
			if(num >= item.lacknum) {
				itemnum = item.lacknum;
				num = num - itemnum;
				item.lacknum = 0; //本单缺货抵消
			} else { //抵消数量小于缺货数量 
				itemnum = num;
				item.lacknum = item.lacknum - num; //本单缺货抵消
				num = 0;
			}
			cost = price1 * itemnum; //	
			console.log('tb_io1,抵消缺口：' + JSON.stringify(item));
			tio1.setObj(item);
			reduceProfit(item.cid, cost)
			if(num == 0) return;
		}
		//num 此时一定要>=0 ,如果小于0说明交易数量为负数，这是程序规则不允许的
	}
	//只有库存大于0的情况下才能盘点，否则没有意义。
	this.reduceStoreLose = function(md, sdate) {
		var mode = md.pors;
		var num = md.num;
		//盘点损失数量+库存数量一定大于0
		if(num == 0) return null;
		var ll = 0; //损溢额
		var qs1 = tstore.where({
			"gid": md.gid
		}).getList();
		var arr = _AW.sort(qs1, "sdate");
		var arrupd = [];
		var arrdel = [];
		
		//console.log('tstore,mode:' + mode)
		//console.log('tstore,arr:' + JSON.stringify(arr))
		if(mode == 'cl1') { //盘点发现损失时
			//如果不够减，插入库存表，记负库存，并全部做成利润
			if(num > 0) { //如果缺货数量大于0 说明库存不够减
				console.log('数据异常： 前端控制当库存-盘点<0 时,不允许盘点');
			}
			//如果库存充足 可以正常减库存计算利润
			//如果库存不足，需要做负库存，直接以采购价=0的计算利润，采购入库时再减利润
			for(var i = 0; i < arr.length; i++) {
				var o = arr[i];
				if(o.num < 0) break; //库存已经不足，无法核减
				// {"ll":ll,lack:0};
				if(o.num >= num) { //如果库存比销售的数量大，只扣销售数量
					ll += num * o.price1;
					var changenum = num;
					o.num = o.num - num;
					num = 0;
					newstore_io(mode, o.gid, o.price1, 0, changenum, sdate, '盘亏-出库');
					if(o.num == 0) {
						tstore.delObjById(o._idkey);
					} else {
						tstore.setObj(o);
					}

					break; // {"ll":ll,lack:0};
				} else { //否则 库存全部减去，继续查询下一条库存
					ll += o.num * o.price1;
					var changenum = o.num;
					num = num - o.num;
					newstore_io(mode, o.gid, o.price1, 0, changenum, sdate, '盘亏-出库');
					tstore.delObjById(o._idkey);

					console.log('tstorelose else：' + num + '’' + ll);
				}
			}

			//return {"lose":ll,"num":num};
		} else if(mode == 'co1') { //
			//盘点增加				
			if(num > 0) { //多出来的数量插入库存表		
				var price1 = this.getGoodPrice( md.gid, 1);
				ll += num * price1;
				newstore_io(mode,  md.gid, price1, 0, num, sdate, '盘盈-入库');
				newstore( md.gid, price1, sdate, num);

			}
		}
		return {
			"lose": ll
		}; //损溢额
	}

	function newstore(gid, price1, sdate, num) {
		var data = clone(MYMODEL['tb_store']);
		data.gid = gid;
		data.price1 = round2(price1, 3);
		data.sdate = sdate;
		data.num = round2(num, 3);
		console.log('newstore:' + JSON.stringify(data))
		tstore.setObj(data);
	}

	function newstore_io(gid, mode, p1, p2, num, sdate, remark) {
		var data = clone(MYMODEL['tb_store_io']);
		data.gid = gid;
		data.pors = mode;
		data.price1 = round2(p1, 3);
		data.price2 = round2(p2, 3);
		data.num = round2(num, 3);
		data.sdate = sdate;
		data.remark = remark;
		console.log('newstore_io:' + JSON.stringify(data))
		tstore_io.setObj(data);
	}

}
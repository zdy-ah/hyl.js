//var URL_SERVERA="http://192.168.1.104:8080/USMD/";
//var URL_SERVERA = "http://192.168.0.114:8080/USMD/";
var URL_SERVERA = "http://47.98.56.33/USMD/";
var URL_BACKUPLOAD = URL_SERVERA + "uploaddb.do";
var URL_BACKDOWNLOAD = URL_SERVERA + "downloaddb.do";
var URL_BACKDBLIST = URL_SERVERA + "backdblist.do";
var URL_IMGCONTRAST = URL_SERVERA + "imgcontrast.do";
var URL_IMGUPLOAD = URL_SERVERA + "imgupload.do";
var URL_IMGDOWNLOAD = URL_SERVERA + "imgdownload.do";
var URL_IMGLIST = URL_SERVERA + "imglist.do";
var URL_IMGREMOVE = URL_SERVERA + "imgremove.do";
var URL_CHECKTOKEN = URL_SERVERA + "checkToken.do";
var URL_checkToken = URL_SERVERA + "checkToken.do";

function checkToken(successcb) {
	mui.ajax(URL_checkToken, {
		dataType: 'json', //服务器返回json格式数据
		type: 'post', //HTTP请求类型
		timeout: 2000, //超时时间设置为10秒；
		success: function(data) {
			if(data == "1") {
				if(successcb)
					successcb();
			}
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			//for (var a in errorThrown)
			alert("网络无法联通，请先检查网络！如有问题请留言客服！");
		}
	});
}

function checkToken(successcb) {
	var sessionid = getItem("sessionuid");
	checkToken(function() {
			mui.ajax(URL_CHECKTOKEN, {
				dataType: 'json', //服务器返回json格式数据
				headers: {
					"Access-Control-Allow-Headers": "X-Requested-With",
					"apikey": "2a4469adb23039b30b55b5970e34f5ac",
					"sessionuid": sessionid
				},
				type: 'post', //HTTP请求类型
				timeout: 10000, //超时时间设置为10秒；
				success: function(data) {
					if(data.head.code == "1") {//如果身份验证通过执行....
						if(successcb)
							successcb();
					}
				},
				error: function(xhr, type, errorThrown) {
					//异常处理；
					//for (var a in errorThrown)
					alert("返回参数异常！");
				}
			});
		}
	);

}
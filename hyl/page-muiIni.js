/**
 * 演示程序当前的 “注册/登录” 等操作，是基于 “本地存储” 完成的
 * 当您要参考这个演示程序进行相关 app 的开发时，
 * 请注意将相关方法调整成 “基于服务端Service” 的实现。
 **/

(function($, owner) {
//mui.toast('载入数据..', { duration: 'short', type: 'div' });
mui.toast('',{isload:1})
	owner._ISDEVELOP_ = '1';
	owner._result={
		code:0,//默认是未改变 0 已改变=1，也可以设置为需要载入的页面别名
	  msg:null,//当前页面给父窗体的返回值
	};
	owner._request=null;
	owner._self = {}; //当前页面
	owner._opener = null; //父窗体页面
	/**
	 * 获取应用本地配置
	 **/
	owner.setSettings = function(settings) {
		settings = settings || {};
		localStorage.setItem('$settings', JSON.stringify(settings));
	}

	/**
	 * 设置应用本地配置
	 **/
	owner.getSettings = function() {
		var settingsText = localStorage.getItem('$settings') || "{}";
		return JSON.parse(settingsText);
	}
	
	/**
	 * 获取本地是否安装客户端
	 **/
	owner.isInstalled = function(id) {	
		if(mui.os.android) {
			var main = plus.android.runtimeMainActivity();
			var packageManager = main.getPackageManager();
			var PackageManager = plus.android.importClass(packageManager)
			var packageName = {
				"qq": "com.tencent.mobileqq",
				"weixin": "com.tencent.mm",
				"sinaweibo": "com.sina.weibo"
			}
			try {
				return packageManager.getPackageInfo(packageName[id], PackageManager.GET_ACTIVITIES);
			} catch(e) {}
		} else {
			switch(id) {
				case "qq":
					var TencentOAuth = plus.ios.import("TencentOAuth");
					return TencentOAuth.iphoneQQInstalled();
				case "weixin":
					var WXApi = plus.ios.import("WXApi");
					return WXApi.isWXAppInstalled()
				case "sinaweibo":
					var SinaAPI = plus.ios.import("WeiboSDK");
					return SinaAPI.isWeiboAppInstalled()
				default:
					break;
			}
		}
	}
	
	
	//快捷方式 取出对象
	owner.q= function (targetid) {
		return document.querySelector(targetid)
	}
	
	owner.g= function (targetid) {
		return document.getElementById(targetid)
	}
	
	owner.gUrlParam=function (name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r != null) {
			return unescape(r[2]);
		} else {
			return null;
		}
	}	
	owner.getRequest=function (){	
		//	console.log('	owner.getRequest1',owner._request)
		if (owner._request)
			return  owner._request;
	  owner._request=	_STO.pop("openwindow");
		//console.log('	owner.getRequest2',owner._request)
		return owner._request;
	}
	//打开窗体 顶部对齐
	owner.open=function (url, data) {
		_STO.push("openwindow",data);
		//console.log('owner.open',data)
 		owner.webview=mui.openWindow({
			url: url,
			id: url,
			waiting: {
				autoshow: true,
				title: '....'
			},
			extras: data || {}, //自定义扩展参数，可以用来处理页面间传值
			createNew: true //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
		}); 
	}	
	
	//打开窗体 带特有风格
	owner.openWithStyle =function (url, data, style) {
		_STO.push("openwindow",data);
		owner.webview=mui.openWindow({
			url: url,
			id: url,
			styles: style,
			extras: data || {}, //自定义扩展参数，可以用来处理页面间传值
			createNew: false //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
		});
	}
	
	//移除焦点
	owner.blur =function () {
		document.activeElement.blur();
	}

	//后退方法
	$.back = function() {
		owner.blur();
		if (typeof mui.options.beforeback === 'function') {
			if (mui.options.beforeback() === false) {
				return;
			}
		}
		//判断当前页面是否改变
		if (owner._result.code && owner._result.code  != 0) {
			//var view = _self.opener();
			//console.log(owner._result);
			//触发父窗体的改变事件
			mui.fire(owner._opener, 'Acknowledge',owner._result);
		}
		mui.doAction('backs');
	};
	$.receive('Acknowledge', function(e) {　 //获取参数值
	 // console.log('$.receive',e);
		//console.log("$.receive",myEvent);
		//alert("receive")
		if (afterWindowAck)
			afterWindowAck(e);		
	});
	//接收请求参数
	owner.getRequest();
}(mui, window.app = {}));

var afterPlusReady = null; //plus准备好以后的回调函数
var afterWindowAck = null; //plus准备好以后的回调函数
var _mydb_ = new DataBase("mydb");
//console.log('page-muiini',_mydb_)
//启动页面

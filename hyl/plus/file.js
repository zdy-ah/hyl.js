/**
 * 文件常用操作函数封装
 */

(function(e,t) {
		if ("object" == typeof exports && "object" == typeof module) module.exports._FI = new t();
		else if ("function" == typeof define && define.amd) define(t);	
		else e._FI = new t(); //对象
})(this,function () {
	var that = this;

	/**
	 * 打开文件
	 * @param {Object} fileurl 文件路径
	 * @param {Object} succesCB 成功后异步回调函数
	 * @param {Object} errorCB 失败后异步回调函数
	 * @param {Object} options 打开选项 {new:true 覆盖 }:{zip:true 压缩  false不压缩 }???
	 */
	this.openFile = function(fileurl, succesCB, errorCB, options) {
		//console.log('zdyfile.openFile'+fileurl)
		var fui = that.analyseUrl(fileurl);
		plus.io.resolveLocalFileSystemURL(fileurl,
			function(entry) {
				if(succesCB) {
					succesCB(entry);
				}
			},
			function(err) {
				plus.io.resolveLocalFileSystemURL(fui.root, function(entry2) {
					entry2.getDirectory(fui.path, {
						create: true
					}, function(dir) { //创建目录后继续创建子文件
						dir.getFile(fui.filename, {
							create: true
						}, succesCB, errorCB);
					});
				});
			}
		);
	};
	/**
	 * 获取文件信息
	 * @param {Object} targetPath 文件路径
	 * @param {Object} success 获取成功后的异步回调函数
	 */
	this.getFileInfo = function(targetPath,success) {
		plus.io.resolveLocalFileSystemURL(targetPath, function(entry) {		
				entry.file(function(fi) {
					if (success)
						success(fi);
				});
			});
	};

	/**
	 * 读取文件
	 * @param {Object} targetPath 文件路径
	 * @param {Object} success 读取成功后的异步回调函数
	 */
	this.readFile = function(targetPath, succesCB) {
		//console.log("readFile="+targetPath);
			plus.io.resolveLocalFileSystemURL(targetPath, function(entry) {		
				//console.log("entry.isFile="+entry.isFile)
				that.readData(entry, succesCB);
			});
	};
	/**
	 * 读取数据
	 * @param {Object} fileurl 文件路径
	 * @param {Object} succesCB 读取成功后的异步回调函数
	 * @param {Object} encode 编码
	 */
	this.readData = function(fileurl,succesCB,encode) {
		encode=encode|'utf-8';
		that.openFile(fileurl,function(entry){
			entry.file(function(fi) {
				var reader = new plus.io.FileReader();
				reader.onload = function(e) {
					if(succesCB) {
						//console.log("succesCB"+JSON.stringify(e));
						succesCB(e.target.result);
					}
				};
				reader.readAsText(fi, encode);
			});
		});
		
	};


	/**
	 * 写入数据
	 * @param {Object} entry 文件对象
	 * @param {Object} str 写入字符串
	 * @param {Object} options {method:0:rewrite|1:append}
	 * @param {Object} successCB  写入成功后的异步回调函数
	 * @param {Object} errorCB 写入失败后的异步回调函数
	 */
	this.writeData = function(entry, str, options, successCB, errorCB) {
		//console.log(entry.toURL())
		entry.createWriter(function(writer) {
			writer.onwrite = function(e) { //写入成功后调哟了
				//console.log("导出数据！"+entry.toURL()+" "+writer.length)	
				//
				//var r=e.target;				
				//r.truncate(writer.length); 引发二次写入完成事件	
				if(successCB)
					successCB(entry);
			};
			if(options.method == 1)
				writer.seek(writer.length);
			else {
				writer.seek(0);			
			}		
			writer.write(str);
		}, function(e) {
			console.log("writeData error!" + e.message);
		});
	};

	/**
	 * 写入数据
	 * @param {Object} fileurl 文件路径
	 * @param {Object} str 写入数据
	 * @param {Object} options {zip:true 压缩  false不压缩,method:0:rewrite|1:append}
	 * @param {Object} successCB  写入成功后的异步回调函数
	 * @param {Object} errorCB 写入失败后的异步回调函数
	 */
	this.writeFile = function(fileurl, str, options, successCB, errorCB) {
		//console.log("writeFile")
	
		options=options|{zip:false,method:0};
		if (options.method==0){
			that.deleteFileByURL(fileurl,function(e){
				that.openFile(fileurl, function(entry) {
					that.writeData(entry, str, options, successCB, errorCB)
				});			
			});
		}else{
			that.openFile(fileurl, function(entry) {
					that.writeData(entry, str, options, successCB, errorCB)
			});			
		}
	};
	/**
	 * 解析url 信息
	 * 全路径，
	 * 根路径，
	 * 子路径，
	 * 文件名， 包含扩展名
	 * 扩展名
	 */
	this.analyseUrl = function(vurl1) {
		var fui = {
			fullpath:'',
			url:'',
			root: '',
			path: '',
			subpath: '',
			filename: '',
			ext: ''
		};
		//console.log("analyseUrl"+JSON.stringify(vurl1))
		var i=vurl1.indexOf("file://");
		var vurl='';
		if (i>=0){
			var i0 = vurl1.indexOf(':')+3;
			fui.fullpath= vurl1.slice(i0) ;
			vurl=plus.io.convertAbsoluteFileSystem( vurl1.slice(i0) );
		}else if (vurl.indexOf("/")==0){			
			fui.fullpath= vurl1 ;
			vurl=plus.io.convertAbsoluteFileSystem( vurl1);
		}else{
			fui.fullpath=  plus.io.convertLocalFileSystemURL( vurl1 ) ;
		 	vurl=vurl1;
		 }
		fui.url=vurl;
	
		//console.log("cc--"+vurl.indexOf("_")+vurl);
		var i1 = vurl.indexOf('/');
		var i2 = vurl.lastIndexOf('/');
		if(i1 < 0) {
			fui.path = fui.root = vurl.slice(0);
		} else {
			fui.root = vurl.slice(0, i1);
			fui.path = vurl.slice(0, i2+1);
			if (i2>i1){
				fui.subpath= vurl.slice(i1+1, i2);
			}			
			fui.filename = vurl.slice(i2+1);			
			fui.ext= that.getExt(fui.filename);
		}
		return fui;
	};
	/**
	 * 获取文件扩展名
	 * @param {Object} filename
	 */
	this.getExt= function(filename){
		var i3=filename.lastIndexOf('.');
		if (i3>0){
			return filename.slice(i3+ 1);
		}
		return '';
	};
	/**
	 * 打开目录
	 * @param {Object} path 目录路径
	 * @param {Object} callback 成功后回调函数
	 */
	this.openDir = function(path, callback) {
		//console.log('zdyfile.openDir'+path);
		var fui = that.analyseUrl(path);
		//console.log("path"+path)
	//	console.log('fui='+JSON.stringify(fui));
		plus.io.resolveLocalFileSystemURL(fui.root, function(root) {
			//console.log('zdyfile.subpath='+fui.subpath)
			if(fui.subpath == null||fui.subpath =='') {
				if(callback)
					callback(root);
			} else {
				root.getDirectory(fui.subpath, {
					create: true
				}, function(dir) { //
					if(callback)
						callback(dir);
					return;
				}, function(e) {console.log(e.message);});
			}
		}, function(e) {
			console.log(e.message+path);
		}
		);
	};


	/**
	 * 删除目录，包含子文件夹
	 * @param {Object} path 目录路径
	 * @param {Object} succesCB 成功回调函数
	 * @param {Object} errorCB 失败回调函数
	 */
	this.deleteDirByURL = function(path, succesCB, errorCB) {			
		plus.io.resolveLocalFileSystemURL(path, function(entry) {
			//console.log("deleteDirByURL"+path);
			that.deleteDir(entry, succesCB, errorCB);
		},function(){
		
			if (succesCB)
				succesCB();
		});
	};
	/**
	 * 删除目录，包含子文件夹
	 * @param {Object} entry 目录对象
	 * @param {Object} succesCB 成功回调函数
	 * @param {Object} errorCB 失败回调函数
	 */
	this.deleteDir = function(entry, succesCB, errorCB) {
		if(entry&&entry.isDirectory)
			entry.removeRecursively(succesCB, errorCB);
	};
	/**
	 * 删除文件
	 * @param {Object} entry 文件对象
	 * @param {Object} succesCB 成功回调函数
	 * @param {Object} errorCB 失败回调函数
	 */
	this.deleteFile = function(entry, succesCB, errorCB) {
		if(entry&&entry.isFile)
			entry.remove(succesCB, errorCB);
	};
	/**
	 * 删除文件
	 * @param {Object} fileurl 文件路径
	 * @param {Object} succesCB 成功回调函数
	 * @param {Object} errorCB 失败回调函数
	 */
	this.deleteFileByURL = function(fileurl, succesCB, errorCB) {
		plus.io.resolveLocalFileSystemURL(fileurl, function(entry) {
			// 可通过entry对象操作test.html文件 
			if(entry.isFile)
				entry.remove(succesCB, errorCB);
		}, function(e) {
			if (succesCB){
				succesCB();
			}
		});
	};
	/**
	 * 压缩
	 * @param {Object} src 源文件路径
	 * @param {Object} tag 目标文件路径
	 * @param {Object} succesCB 成功回调函数
	 * @param {Object} errorCB 失败回调函数
	 */
	this.zip=function(src,tag,success,error){
		plus.zip.compress(src, tag,success,error);
	};
	/**
	 * 解压缩
	 * @param {Object} src 源文件路径
	 * @param {Object} tag 目标文件路径
	 * @param {Object} succesCB 成功回调函数
	 * @param {Object} errorCB 失败回调函数
	 */
	this.unzip=function(src,tag,success,error){
		plus.zip.decompress(src, tag,success,error);
	};

	/**
	 * 压缩图片
	 * @param {Object} vsrc 文件路径
	 * @param {Object} vtag 目标文件名
	 * @param {Object} success 成功回调函数
	 */
	this.zipimg=function(vsrc,vtag,success){
		//console.log(vsrc);
		plus.zip.compressImage({
			src:vsrc,
			dst:vtag,
			quality:50
		},
		function(e) {
			if(success){
				success(vtag);
			}
		},function(error) {
			alert("Compress error!");
		});
	};

	/**
	 * @param {Object} param param.formpath 从什么路径 ;param.topath 到什么路径; param.tofile; 重命名
	 * @param {Object} tagcallback  拷贝完成 源文件如何处理
	 * @param {Object} srccallback 拷贝完成 目标文件如何处理
	 */
	this.move = function(param, tagcallback, srccallback) {
		var formpath = param.formpath;
		var topath = param.topath;
		var tofile = param.tofile;		
//		console.log("formpath="+formpath)
//		console.log("topath="+topath);
//		console.log("tofile="+tofile);
		plus.io.resolveLocalFileSystemURL(formpath, function(entry) {
			//目标路径
			//console.log('move 1');
			that.openDir(topath, function(root) {
				//console.log('move 0');
				root.getFile(tofile, {}, function(file) {
						//console.log('move'+file.toURL());
						//console.log('move 2');
						file.remove(function() {
							entry.copyTo(root, tofile, function(toentry) {
								//console.log('move 3');
								if(tagcallback)
									tagcallback(toentry, param);
								if(srccallback)
									srccallback(entry, param);
							}, function(e) {
								console.log("拷贝文件错误:" + e.message);
							});
						}, function(e) {
							console.log("移除文件错误:" + e.message);
						});
					},
					function(e) {
							//console.log('move 4');
						entry.copyTo(root, tofile, function(toentry) {
							//console.log('move 5'+toentry.fullPath);
							if(tagcallback)
								tagcallback(toentry, param);
							if(srccallback)
								srccallback(entry, param);
						}, function(e2) {
							console.log("拷贝文件错误:" + entry.toURL() );
							console.log("拷贝文件错误:" + root );
							console.log("拷贝文件错误:" + tofile );
						});
					});
			}, function(e) {
				console.log("读取目标文件错误：" + e.message);
			});
		}, function(e) {
			console.log(formpath + ',' + topath + ',' + tofile)
			console.log("读取拍照文件错误：" + e.message);
		});
	}
	/**
	 * 路径转url
	 * @param {Object} path
	 */
	this.toUrl=function(path){
	    if (path==null) return null;
		if (path.indexOf('://')>0)
			path=path.replace("file://","")
		return plus.io.convertAbsoluteFileSystem(path);
	}

}

);
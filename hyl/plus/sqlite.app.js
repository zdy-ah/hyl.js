/*"SQLite": {
		"description": "SQLite数据库"
	}*/
/**
 * 用于连接app的sqlite 
 *	@param {Object} dbname
 */
(function(e, t) {
	if ("object" == typeof exports && "object" == typeof module) module.exports.DataBase = t;
	else if ("function" == typeof define && define.amd) define(t);
	else e.DataBase = t; //静态函数库
})(this, function(dbname) {
	var _dbname = dbname;
	var _dburl = '_doc/' + dbname + '.db';

	function isOpened() {
		var that = this;
		if (plus.sqlite.isOpenDatabase({
				name: _dbname,
				path: _dburl,
			})) {
			//console.log("1111")
			return 1;
		} else {
			//console.log("0000")
			return 0;
		}

	}

	// 打开数据库
	function open(sf) {
		var that = this;
		if (isOpened() == 0) {
			plus.sqlite.openDatabase({
				name: _dbname,
				path: _dburl,
				success: sf || function(e) {
					console.log('打开数据库 ok: ' + dbname);
				},
				fail: function(e) {
					console.log('打开数据库 fail: ' + JSON.stringify(e));
				}
			});
		} else {
			if (sf != null) sf();
		}
	};
	// 关闭数据库
	function close() {
		var that = this;
		plus.sqlite.closeDatabase({
			name: _dbname,
			success: function(e) {
				console.log('关闭数据库 ok: ');
			},
			fail: function(e) {
				console.log('关闭数据库 fail: ', e);
			}
		});
	};


	this.exesql = function(sql, callback, onFail) {
		plus.sqlite.executeSql({
			name: _dbname,
			sql: sql,
			success: function(e) {
				if (callback) callback(e, sql);
			},
			fail: onFail || function(e) {
				console.log('exec fail: ' + sql, e)
			}
		})
	}
	this.exec = function(sql, onFinished, onFail) {
		var that = this;
		if (_IS.isEmpty(sql)) return;
		open(function() {
			that.exesql(sql, function(e, sql) {
				if (onFinished) onFinished(e, sql);
				close();
			}, onFail);
		})

	};

	this.querysql = function(sql, callback, onFail) {
		plus.sqlite.selectSql({
			name: _dbname,
			sql: sql,
			success: function(e) {
				if (callback) callback(e, sql);
			},
			fail: onFail || function(e) {
				console.log('query fail: ' + sql, e)
			}
		})
	}


	// 查询SQL语句
	this.query = function(sql, onFinished, onFail) {
		var that = this;
		if (_IS.isEmpty(sql)) return;
		open(function() {
			that.querysql(sql, function(e, sql) {
				if (onFinished) onFinished(e, sql);
				close();
			}, onFail);
		});
	};

	this.commit = function(callback, onFail) {
		plus.sqlite.transaction({
			name: _dbname,
			operation: 'commit',
			success: function(e) {
				close();
				if (callback) callback();
				console.log('事务成功提交');
			},
			fail: function(e) {
				plus.sqlite.transaction({
					name: name,
					operation: 'rollback',
					success: function(e) {
						close();
						console.log('回滚结束');
					},
					fail: onFail || function(e) {
						close();
						console.log('回滚失败');
					}
				});
			}
		});
	}




	function appendSql(sqls, onFinished, onFail) {
		var sql = sqls[0];
		if (_IS.isEmpty(sql)) {
			if (onFinished) onFinished();
			return;
		}
		var f = function() {
			var sqls2 = sqls.slice(1);
			appendSql(sqls2, onFinished, onFail);
		}
		if (sql.trim().startWith("select")) {
			that.querysql(sql, f, onFail);
		} else {
			that.exesql(sql, f, onFail);
		}

	}
	/**
	 * batch 开启事务批量顺序执行sql脚本集合
	 * @param {Array} sqls 数组形式传入
	 * @param {Function} onFinished 全部执行完毕，提交后调用
	 */
	this.batch = function(sqls, onFinished, onFail) {
		var that = this;
		if (_IS.isArray(sqls)) {
			open(function() {
				plus.sqlite.transaction({
					name: _dbname,
					operation: 'begin',
					success: function(e) {
						appendSql(sqls, function() {
							that.commit(function() {
								if (onFinished) onFinished();
							});
							console.log("批处理结束");
						}, onFail);
					},
					fail: onFail || function(e) {
						close();
						console.log('事务开启失败: ' + JSON.stringify(e));
					}
				});
			});
		}
	}

	/**
	 * transaction 可以支持复杂事务方法，
	 * 用法：
	_DBM.transaction(function(){
		_DBM.execsql(function (){
			_DBM.execsql(function (){
				_DBM.execsql(function (){
					_DBM.commit();
				});
			});
		});
	});
	*/
	// 执行事务， 只要sqls 中有一个错误都不执行
	this.transaction = function(onRunning, onFail) {
		open(function() {
			plus.sqlite.transaction({
				name: _dbname,
				operation: 'begin',
				success: function(e) {
					if (onRunning) onRunning();
				},
				fail: onFail || function(e) {
					close();
					console.log('事务开启失败: ' + JSON.stringify(e));
				}
			});
		});
	}


});
//export { mysqlite}

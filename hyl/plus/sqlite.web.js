/**
 * 阿有 email:37798955@qq.com
 * 用于连接浏览器的sqlite ，有的浏览器没有sqlite 例如firefox
 */
(function(e, t) {
	if ("object" == typeof exports && "object" == typeof module) module.exports.DataBase = t;
	else if ("function" == typeof define && define.amd) define(t);
	else e.DataBase = t; //需要实例化
})(this, function(dbname) {
	var _dbname = dbname;
	var _dburl = dbname + '.db';
	this._db = null;

	function open(fs) {
		var that = this;
		if (!window.openDatabase) { //firefox 不支持sqlite数据库
			//console.log('openDb fail: ');
			return null;
		} else {
			var db = openDatabase(dbname, '1.0', _dburl, 50 * 1024 * 1024);		
			return db;
		}
	}

	function getRs(rows) {
		var list = [];
		var len = rows.length,
			i
		for (i = 0; i < len; i++) {
			list.push(rows.item(i));
		}
		return list;
	}
	async function asynexec(t, sql, callback, onFail) {
		if (_IS.isEmpty(sql)) return;
		t.executeSql(sql, null, function(tx, results) {
			if (callback)
				callback(results, sql);
			//console.log("exec:" + sql);
		}, onFail || function(tx, error) {
			console.log("error:", sql, error.message);
		});
	}
	async function asynquery(t, sql, callback, onFail) {
		if (_IS.isEmpty(sql)) return;
		t.executeSql(sql, null, function(tx, results) {
			if (callback)
				callback(getRs(results.rows), sql);
			//console.log("query:" + sql);
		}, onFail || function(tx, error) {
			console.log("error:", sql, error.message);
		});
	}
	//查询，内部方法
	this.exec = function(sql, callback, onFail) {
		var that = this;
		that._db.transaction(function(t) {
			asynexec(t, sql, callback, onFail);
			console.log("exec::" + sql);
		})
	}
	this.query = function(sql, callback, onFail) {
		var that = this;
		that._db.transaction(function(t) {
			asynquery(t, sql, callback, onFail);
			console.log("query::" + sql);
		})
	}
	/** 区别于事务方式，即使sqls 中有错误，也会继续执行其他的
	 * @param {Object} sqls
	 * @param {Object} callback
	 */
	this.batch = function(sqls, onFinished, onFail) {
		var that = this;
		that._db.transaction(function(tx) {
			appendSql(tx, sqls, onFinished, onFail);
		});
	}

	function appendSql(tx, sqls, onFinished, onFail) {
		var that = this;
		var sql = sqls[0];
		if (_IS.isEmpty(sql)) {
			if (onFinished) onFinished({}, "batch sql");
			return;
		}
		tx.executeSql(sql, null, function(tx, results) {
			var sqls2 = sqls.slice(1);
			appendSql(tx, sqls2, onFinished);
		}, onFail || function(tx, error) {
			console.log("transaction:error:", sql, error.message);
		});
	}
	this.commit = function(callback) {
		if (callback) callback();
	};
	/**
	 *  中有一个错误都不执行
	 * @param {Object} sqls
	 * @param {Object} callback
	 */
	this.transaction = function(onRunning) {
		var that = this;
		that._db.transaction(function(tx) {
			if (onRunning == undefined) return;
			//console.log(onRunning)
			onRunning(tx);
		});
	}
	this._db = open();
});

/*
examples:
var db=createdb();
db.addObj('channel_list',[{name:'id',type:'integer primary key autoincrement'},{name:'name',type:'text'},{name:'link',type:'text'},{name:'cover',type:'text'},{name:'updatetime',type:'integer'},{name:'orders',type:'integer'}]);
db.init('feed_list',[{name:'parentid',type:'integer'},{name:'feed',type:'text'}]);
db.where({name:'aa'}).getData(function(result){
  console.log(result);//result为Array
});
db.where({name:'aa'}).deleteData(function(result){
  console.log(result[0]);//删除条数
});
db.where({name:'bb'}).saveObj({link:'jj'},function(result){
  console.log(result);//影响条数
})
})
*/

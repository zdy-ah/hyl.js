(function(e,t) {
		if ("object" == typeof exports && "object" == typeof module) module.exports._STO =new t();
		else if ("function" == typeof define && define.amd) define(t);	
		else e._STO =new t(); //对象
})(this,function () {
	
		this.getLen=function () {
			if(window.plus)
				return plus.storage.getLength();
			else
				return window.localStorage.length;
		};
		
		this.removeItem=function (key) {
			if(window.plus)
				plus.storage.removeItem(key);
			else
				window.localStorage.removeItem(key);
		};
		
		this.key=function (index) {
			if(window.plus)
				plus.storage.key(index);
			else
				window.localStorage.key(index);
		};
		this.setItem=function (key, value) {
			if(key) {
				if(window.plus) {					
					plus.storage.setItem(key, JSON.stringify(value));
				} else {
					window.localStorage[key] = JSON.stringify(value);
				}
			}
		};
		
		this.getItem=function (key) {
			if(key) {
				var obj = null;
				if(window.plus) {					
					obj = plus.storage.getItem(key);
				} else {
					obj = window.localStorage[key];
				}
				if(obj)
					return JSON.parse(obj);
			}
			return null;
		}	
		
		this.push=function(key,value){
			var arr=this.getItem(key)||[];
			arr.push(value);
			this.setItem(key,arr);
		}
		
		this.pop=function(key){
			var arr=this.getItem(key)||[];		
			var res=arr.pop();
			this.setItem(key,arr);
			return res;
		}
	}
);
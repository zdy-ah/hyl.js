/**
 * 阿有 email:37798955@qq.com
 * 2017-11
 */

//menudata 对象必须有text属性和value属性
function loadPicker(tagid, menuData, callback) {
	function menuPickerEvent(event) {
		menuPicker.show(function(items) {
			if(items[0].text) {
				tag.value = items[0].text; //+ " " + items[1].text;
				tag.extenddata = items[0].value; //选中的id
				//console.log(items[0].value)
				if(callback)
					callback(tag.value);
			}
		});
	}
	var tag = document.getElementById(tagid);
	var menuPicker = null;
	//console.log(tag.picker)
	//console.log("menuData"+JSON.parse(menuData))
	if(tag.picker) {
		menuPicker = tag.picker
		menuPicker.setData(menuData);
		//
	} else {
		menuPicker = new mui.PopPicker();
		tag.picker = menuPicker;
		//console.log(tag.picker)
		menuPicker.setData(menuData);
		//tag.removeEventListener("tap", menuPickerEvent);
		tag.addEventListener('tap', menuPickerEvent, false);
	}

	//console.log('loadpicker.8:'+tag)

	return menuPicker;
}
function getPickerData2(t0s,t1s){
	var j=0;
	var menuData=[];
	for (var i=0;i < t0s.length;i++){
		var a=clone(t0s[i]);		
		if (!('value0' in a ))
			a['value0']=a['text0'];
		a['children']=[];	
		var c=a['children'];
		for(;j<t1s.length;j++){
			var b=t1s[j]
			if (b['text0']==a['text0']){
				var d={}
				d['value1']=b['value1'];
				d['text1']=b['text1'];
				c.push(d)
			}
			else{			
				break;
			}
		}
		menuData.push(a);
	}
    //console.log(menuData);
    return menuData;
}
function loadPicker2(tagid, menuData, callback) {
	function menuPickerEvent(event) {
		menuPicker.show(function(items) {
			if(items[1].text) {
				tag.value = items[1].text; //+ " " + items[1].text;
				tag.extenddata = items[1].value; //选中的id
				//console.log(items[0].value)
				if(callback)
					callback(tag.value);
			}
		});
	}
	var tag = document.getElementById(tagid);
	var menuPicker = null;
	//console.log(tag.picker)
	//console.log("menuData"+JSON.parse(menuData))
	if(tag.picker) {
		menuPicker = tag.picker
		menuPicker.setData(menuData);
		//
	} else {
		menuPicker = new mui.PopPicker({
						layer: 2
				});	
		tag.picker = menuPicker;
		//console.log(tag.picker)
		menuPicker.setData(menuData);
		//tag.removeEventListener("tap", menuPickerEvent);
		tag.addEventListener('tap', menuPickerEvent, false);
	}
	return menuPicker;
}
//tagid 目标对象
//format in "datetime,time,date,month,hour"
function loadDateTimePicker(tagid, format, callback) {
	function dateTimePickerEvent() {
		var _self = this;
		if(_self.picker) {
			_self.picker.show(function(rs) {
				_self.value = rs.text;
				_self.picker.dispose();
				_self.picker = null;
			});
		} else {
			//	var optionsJson = '{"value":"2015-10-10 10:10","beginYear":2010,"endYear":2020}' ;
			var options = {}
			if(format == null || format == '') {
				options.type = 'datatime'
			} else {
				options.type = format
			}
			//JSON.parse(optionsJson);						
			_self.picker = new mui.DtPicker(options);
			_self.picker.show(function(rs) {
				_self.value = rs.text;
				if(callback)
					callback(rs.text)
				_self.picker.dispose();
				_self.picker = null;
			});
		}
	}
	var databtn = document.getElementById(tagid);
	//databtn.removeEventListener("tap", dateTimePickerEvent);
	databtn.addEventListener('tap', dateTimePickerEvent, false);

}
/**
 * 阿有 email:37798955@qq.com
 * 2017-11
 */

function loadscript(id, url, type) {
	id = 'script' + id
	var oHead = document.getElementsByTagName('HEAD').item(0);
	var scriptTag = document.getElementById(id);
	if(scriptTag) oHead.removeChild(scriptTag);
	var oScript = document.createElement("script");
	oScript.id = id;
	if(type == null)
		type = "text/javascript"
	oScript.type = type;
	if(url != null) {
		oScript.src = url;
	}
	oHead.appendChild(oScript);
}

function loadTpl2Div(tpl, data, target) {
	loadscript(tpl, null, 'text/html')
	mui("#script" + tpl).load(tpl + '.html', function() {
		var html = template('script' + tpl, data);
		var tag = document.getElementById(target);
		tag.innerHTML = html;
	});
}

function tpl2Div(tpl, data, target) {
	var tag = document.getElementById(target);
	if(tag == null) return null;
	var html = template(tpl, data);
	tag.innerHTML = html;
}

var MYMODEL = {
tb_types:{
_data :''
},

tb_users:{
ulogin :'',
upass :'',
autho :'',
url :'',
token :'',
days :0,
tel :'',
status :1
},

tb_users1:{
uid :'',
uname :'',
sex :'',
motto :'',
uimg :'',
uqq :'',
weixin :'',
addr :'',
zipc :'',
email :'',
remark :''
},

tb_contacts:{
cid :'',
pysx :'',
djrq :'',
cgroup :'',
cclass :'',
clevel :'',
uname :'',
tel :'',
uqq :'',
weixin :'',
addr :'',
email :'',
remark :'',
zipc :'',
uimg :'',
sex :'',
status :1
},

tb_goods:{
gid :'',
gname :'',
gclass :'',
pysx :'',
spec :'',
weight :0,
cbcm :0,
gaddr :'',
price1 :0,
price2 :0,
levelprices :[],
gnote :'',
gimg :'',
quality :0,
status :1
},

tb_io:{
pid :'',
cid :'',
pors :0,
amount :0,
tpnum :0,
allnum :0,
allweight :0,
allsize :0,
sdate :'',
edate :'',
remark :'',
status :1
},

tb_io1:{
did :'',
cid :'',
pid :'',
gid :'',
price :0,
num :0,
pors :0,
damount :0,
weight :0,
size :0,
remark :'',
lacknum :0,
ldate :'',
status :1
},

tb_checknum:{
did :'',
gid :'',
oldnum :0,
num :0,
dvalue :0,
price :'',
sdate :'',
remark :''
},

tb_store_io:{
pors :0,
gid :'',
price1 :0,
price2 :0,
num :0,
sdate :'',
remark :''
},

tb_store:{
gid :'',
price1 :0,
num :0,
sdate :''
},

tb_price_mon:{
monnum :0,
cid :'',
gid :'',
num :0,
price :0,
pricel :0,
priceh :0
},

tb_store_mon:{
monnum :'',
gid :'',
initnum :0,
num :0,
rsnum :0,
rbnum :0,
bnum :0,
snum :0,
synum :0,
samout :0,
camout :0,
profit :0,
lose :0
},

tb_journal:{
jid :'',
cid :'',
mode :'',
amount :0,
receivable :0,
payable :0,
capital :0,
edate :'',
remark :'',
monnum :''
},

tb_myacct:{
monnum :'',
receivable :0,
payable :0,
capital :0,
camout :0,
samout :0,
income :0,
expend :0,
lose :0,
profit :0
},

tb_tradeacct:{
monnum :0,
cid :'',
receivable :0,
payable :0,
income :0,
expend :0,
profit :0,
samout :0,
camout :0
},

tb_settle:{
reid :'',
cid :'',
mode :'',
amount :0,
reduces :0,
discount :0,
paid :0,
paytype :'',
sdate :'',
remark :'',
status :1
}

}
//固定字典
var Dict_cgroup = [{
		text: '未分组',
		value: '0'
	},
	{
		text: '上家(供应商)',
		value: '1'
	},
	{
		text: '下家(代理)',
		value: '2'
	}
];
var Dict_tradeType = [{
		text: '未知',
		value: '0'
	},
	{
		text: '采购',
		value: '1'
	},
	{
		text: '销售',
		value: '2'
	}
];
var Dict_acctType = {
	N:'未知',a1:'红冲存款',a2:'红冲应收款',a3:'红冲应付款'
	,s1:'出库凭证',s2:'收款凭证'
	,b1:'进货凭证',b2:'付款凭证'
	,rb1:'采购退货凭证',rs1:'销售退货凭证'
};
var Dict_payType = [{
		text: '其他',
		value: '0'
	},
	{
		text: '现金',
		value: '1'
	},
	{
		text: '支付宝',
		value: '2'
	},
	{
		text: '微信',
		value: '3'
	},
	{
		text: '银行转账',
		value: '4'
	}
];
var	账户={
		eff:{"存款":1 ,"应付款":2 ,"应收款":3 ,"货款":4}
};
/**
 * 阿有 email:37798955@qq.com
 * 2017-11
 * 配合侧滑框架使用
 * 放在末尾
 */
//侧滑容器父节点
var offCanvasWrapper = mui('#offCanvasWrapper');
document.getElementById('offCanvasShow').addEventListener('tap', function() {
	offCanvasWrapper.offCanvas('show');
	
});

//主界面和侧滑菜单界面均支持区域滚动；
mui('#offCanvasSideScroll').scroll();
mui('#offCanvasContentScroll').scroll();

//var pullrefresh = document.getElementById('offCanvasContentScroll')
//pullrefresh.addEventListener("swipedown", function() {
//	plus.nativeUI.showWaiting("加载中");
//	alert('刷新')
//	pullRefresh()
//	plus.nativeUI.closeWaiting();
//});

//实现ios平台的侧滑关闭页面；
/**
 * 阿有 email:37798955@qq.com
 * 2017-11
 */
var as = 'pop-in'; // 默认动画类型
var IMGTMPPATH = '_doc/tmpimgs/'; //工作目录 拍照结果
var IMGCHGPATH = '_doc/tmpimgs/chg/'; //图片处理时的工作目录
var IMGSAVEPATH = '_doc/upimgs/'; // 图片存储目录

//清空 IMGTMPPATH	
_fi.deleteDir(IMGTMPPATH);

var subpageurl = ["../../plus/camera_image.html", "../../plus/changeimg.html"];

function Eimg() {

	this.src = ""; //路径
	this.name = ""; //文件名称
	this.stamp = ""; //时间戳	
	this.type = ""; //文件类型
	this.size = 0; //大小

	//this.md5="";
}

function SliderImage(pdiv) {
	//当前选中图的index
	var PARENTDIV = pdiv; //容器id

	var vi = new Vue({
		el: '#' + PARENTDIV,
		data: {
			CURINDEX: 0,
			imgs: []
		},
		methods: {
			onclick: function(e) {
				//console.log(e.currentTarget.src);
				if(e)
					showimg(e.currentTarget.src);
			}
		}, //更新
		updated: function() {
			mui('.mui-slider').slider({});

		}
	});

	vi.$watch('CURINDEX', function(newVal, oldVal) {
		var gallery = mui('.mui-slider').slider({});
		if(gallery)
			gallery.gotoItem(newVal);
		/*if(newVal > -1) {
			var gallery = mui('.mui-slider');
			
		}*/
	});

	function showimg(src) {
		if(src) {

			mui.openWindow({
				url: subpageurl[0],
				id: subpageurl[0],
				extras: {
					url: src
				}, //自定义扩展参数，可以用来处理页面间传值
				createNew: false //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
			});
		}
	}
	this.setIndex = function(i) {
		vi.CURINDEX = i;
		//console.log(vi.CURINDEX);
	}
	this.getIndex = function() {
		return vi.CURINDEX;
	}

	//创建图片 输入文件路径
	function creatimage(fpath) {
		var e = new Eimg();
		_fi.getFileInfo(fpath, function(fs) {
			e.src = plus.io.convertLocalFileSystemURL(fpath);
			e.name = fs.name; // 文件数据对象的名称，不包括路径
			e.stamp = fs.lastModifiedDate; //文件对象的最后修改时间
			//console.log("fs.size"+fs.size)
			e.size = fs.size; // 文件数据对象的数据大小，单位为字节
			//console.log("fs.size" + e.size)
			e.type = fs.type; //文件数据对象MIME类型
			//this.md5=hex_md5(fs.slice(0, 1024)); //计算MD5
			vi.imgs.push(e);
			//console.log(e.src + " " + e.size);
			vi.CURINDEX = vi.imgs.length - 1;
		});

	}
	//清空
	function clearImages() {
		vi.imgs = [];
		vi.CURINDEX = -1;
	}
	//删除图片
	this.delImage = function() {
		vi.imgs.splice(vi.CURINDEX, 1);
		if(vi.CURINDEX > 0)
			vi.CURINDEX--;
		else
			vi.CURINDEX = 0;
	}

	//设定置顶照片
	this.lockImage = function() {
		a = vi.imgs[vi.CURINDEX];
		vi.imgs.splice(vi.CURINDEX, 1);
		vi.imgs.unshift(a);
		vi.CURINDEX = 0;
	}
	//获取当前的图片
	function getCurImage() {
		return vi.imgs[vi.CURINDEX];
	}

	//修改图片
	this.chgImage = function(chgimage_htm) {
		var img = getCurImage();
		//console.log("ppp"+urls);
		if(img == null) return;
		mui.openWindow({
			url: subpageurl[1],
			id: subpageurl[1],
			extras: {
				url: img
			}, //自定义扩展参数，可以用来处理页面间传值
			createNew: false //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
		});
	}
	//chgimage_htm 页面修改图片的回调函数
	this.chgCurImgCallback = function(url) {
		var imgt = getCurImage();
		console.log('chgCurImgCallback'+vi.CURINDEX+':'+url)
		var e = new Eimg();
			e.src = url;
		_fi.getFileInfo(url, function(fs) {		
			e.name = fs.name; // 文件数据对象的名称，不包括路径
			e.stamp = fs.lastModifiedDate; //文件对象的最后修改时间
			//console.log("fs.size"+fs.size)
			e.size = fs.size; // 文件数据对象的数据大小，单位为字节
			//console.log("fs" + JSON.stringify(e));
			e.type = fs.type; //文件数据对象MIME类型
			//this.md5=hex_md5(fs.slice(0, 1024)); //计算MD5		
			vi.imgs.splice(vi.CURINDEX,1,e) ;//[] = e;
		});
	
	}

	//保存图片到数据库
	//如果数据库没有图片 就上传，如果有就不上传
	//callback 保存完成后的回调函数
	this.saveImgs = function(callback) {
		var imglist = vi.imgs;
		if(imglist.length == 0) { //如果没有图片退出
			if(callback) {
				callback('')
				return;
			}
		}
		var stats = [];
		//console.log("myplus136--" + arr.length)
		//遍历所有图片
		for(var i = 0; i < imglist.length; i++) {
			//if 图片文件名已经存在，then 不上传  else 上传	
			
			stats.push(0);
			var url = _fi.toUrl(imglist[i].src);
			//console.log("dd"+url);
			if(url.indexOf(IMGSAVEPATH) < 0) //如果图片不在产品保存区，就移到保存区
			{
				var param = getParam(url, IMGSAVEPATH, imglist[i].name);
				param.arri = i;
				_fi.move(param, function(fs, arri) {
					stats[param.arri] = 1;
					vi.imgs[param.arri].src = fs.toLocalURL(); //图片src 指向新的路径
				});
			} else {
				stats[i] = 1;
			}
			//坑:copy 的执行是在for循环全部结束之后 所以i=4 ,必须提前保存i
			//param 前三个参数formpath,topath,filename 不能修改,后面可以改
		}

		//所有图片都保存到保存区以后才能 统一提交表单到数据库
		asynexec(
			function() {
				//console.log("myplus162:--" + arr.length)
				//log(arr,'myplus167')
				for(var i = 0; i < stats.length; i++) {
					if(stats[i] == 0)
						return false;
				}
				return true;
			},
			function() {
				if(callback)
					callback(vi.imgs);
			}, 100
		);

	}
	//异步转同步的函数
	function arrExec(arr, n, callback) {
		if(arr.length < n) {
			setTimeout(function() {
				arrExec(arr, n, callback);
			}, 20);
			//console.log("setTimeout--"+arr.length)
		} else { //完成图片保存保存
			if(callback)
				callback(arr.join(','));
		}
	}

	function asynexec(whereFunc, callback, ms) {
		var flag = false;
		if(whereFunc && whereFunc()) {
			if(callback)
				callback();
		} else {
			if(ms == null) ms = 20;
			setTimeout(function() {
				asynexec(whereFunc, callback, ms);
			}, ms);
		}
	}

	//从数据库载入图片
	//callback 载入完成后的函数
	this.loadImgs = function(vimgs, callback) {
		clearImages();
		if(vimgs == null ) return;
		if (_IS.isArray( vimgs))
			vi.imgs=vimgs;
		else
			vi.imgs=[];
			
		if (vimgs.length>0){
			vi.CURINDEX=0;
			if(callback) callback();
			return;
		}
	}

	//从图片文件夹中选择图片
	this.openImage = function() {
		plus.gallery.pick(function(path) {
			//重命名文件
			var filename = randomname() + path.slice(path.lastIndexOf('.'))

			_fi.zipimg(path, IMGTMPPATH + filename, function(tagfilepath) {
				creatimage(tagfilepath);
			});
		}, function(err) {
			console.log('打开图片异常:' + JSON.stringify(err) + path);
		}, {
			filter: 'image',
			multiple: false
		});
	}
	// 拍照
	this.takeImage = function() {
		//outSet('开始拍照：');	
		var cmr = plus.camera.getCamera();
		cmr.captureImage(function(p) {
			//console.log(p)
			var filename = randomname() + p.slice(p.lastIndexOf('.'))
			_fi.zipimg(p, IMGTMPPATH + filename, function(tagfilepath) {
				creatimage(tagfilepath);
			});

		}, function(e) {
			outLine('读取拍照文件异常：' + e.message);
		}, {
			filename: IMGTMPPATH,
			index: 1
		});
	}
	
	//alert("tt")
}

function getParam(formpath, topath, tofile) {
	var param = {}
	param.formpath = formpath;
	param.topath = topath;
	param.tofile = tofile;
	return param;
}

function imgLoaded(that) {
	var img = that;
	var oimg = new Image();
	oimg.src = img.src
	var b = that.parentNode.parentNode;
	var pb = b.clientHeight / b.clientWidth,
		pi = oimg.height / oimg.width;
	if(pb > pi) {
		img.style.width = b.clientWidth + 'px';
		img.style.height = parseInt(b.clientWidth * pi) + 'px';
	} else {
		img.style.height = b.clientHeight + 'px';
		img.style.width = parseInt(b.clientHeight / pi) + 'px';

	}
	oimg = null;
	b.style.lineHeight = b.clientHeight + 'px';
}

//随机名称
function randomname() {
	var mydate = new Date;
	return "img" + mydate.getFullYear() + (mydate.getMonth() + 1) + '' + mydate.getDate() + '' + mydate.getHours() + mydate.getMinutes() + mydate.getSeconds() + mydate.getMilliseconds() + Math.round(Math.random() * 10000);
}

var _si = null;
//var _scan = new zdyscan();
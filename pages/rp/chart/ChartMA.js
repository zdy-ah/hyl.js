//BMA、总收支利润图：分析收益与利润 -(开始月-截止月)-规律和趋势


function getBar_ma_m(arr1) {
	//收支利润总图
	if (arr1==null||arr1.length==0)
		arr1=randomarray('monnum',['201801','201802','201803','201804','201805','201806','201807','201808','201809'],'camout,expend,samout,profit,income')
	//console.log(JSON.stringify(arr1))
	var md = {
		周期: _AW.getColList(arr1, 'monnum', function(e) {
			var v = Number(e.substr(4, 2));
			if(v % 2 == 0) return '\n' + v;
			return v + '月'
		}),
		采购: _AW.getColList(arr1, 'camout', function(e) {
			return 0 - e
		}),
		销售: _AW.getColList(arr1, 'samout'),
		利润: _AW.getColList(arr1, 'profit'),
		收入: _AW.getColList(arr1, 'income'),
		支出: _AW.getColList(arr1, 'expend', function(e) {
			return 0 - e
		}),
	};
	var barwidth = 5;
	var option= {
		color: _BARTHEME[5],
		legend: {
			data: ['采购', '销售', '利润', '收入', '支出'],
			itemWidth: barwidth,
			//align:'right'
			x:'right',
		  	y:'24'
		},
		toolbox : cssbarbox,	
		grid: cssgrid,
		dataZoom: cssdataZoom,
		calculable: true,
		xAxis: {
			type: 'category',
			'axisLabel': {
				'interval': 0
			},
			data: md.周期,

			splitLine: {
				show: false
			},
			//nameLocation: 'end', // 坐标轴名字位置，支持'start' | 'end'
			scale: true,
			//name: '月份',
		},
		yAxis: [{
			type: 'value',
			nameLocation: 'end', // 坐标轴名字位置，支持'start' | 'end'
			scale: true,
			name: '（元）',

		}],
		series: [{
				name: '采购',
				type: 'bar',
				barWidth: barwidth,
				stack: 'one',
				data: md.采购,
			},
			{
				name: '销售',
				type: 'bar',
				barWidth: barwidth,
				stack: 'one',
				data: md.销售
			}, {
				name: '支出',
				type: 'bar',
				barWidth: barwidth,
				stack: 'two',
				data: md.支出
			},
			{
				name: '收入',
				type: 'bar',
				barWidth: barwidth,
				stack: 'two',
				data: md.收入
			}, {
				name: '利润',
				type: 'line',
				barWidth: barwidth,
				data: md.利润,
				label: {
					normal: {
						show: true,
						position: 'top'
					}
				},
				markLine: {
					data: [{
						type: 'average',
						name: '平均值'
					}]
				},
				markPoint: {
					data: [{
							type: 'max',
							name: '最大值'
						},
						{
							type: 'min',
							name: '最小值'
						}
					]
				},
				areaStyle: {
					normal: {}
				},
			}
		]
	};
	return {
		title:charttitle.bmam,
		option:option
	}
	
}

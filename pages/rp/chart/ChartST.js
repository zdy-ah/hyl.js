//PGD：XX客户-某时段-商品销售分布
//{cid:'xxx',monnum[:'',monnum]:'',gid:'xxx' }


//按月分组  tb_store_mon
function querySt_M(arr1){	
	var arr2=_AW.groupby(arr1,'monnum','samout,camout,profit,num,snum,bnum,rsnum,rbnum');
	var arr3=[];
	for(var i=0;i<arr2.length;i++){
		var o ={};	
		o['monnum']=arr2[i]['monnum']	;
		o['samout']=round2(arr2[i]['samout']['sum'],3);		
		o['camout']=0-round2(arr2[i]['camout']['sum'],3);	
		o['profit']=round2(arr2[i]['profit']['sum'],3);	
		o['num']=round2(arr2[i]['num']['sum'],3);		
		o['snum']=round2(arr2[i]['rsnum']['sum'],3)-round2(arr2[i]['snum']['sum'],3);		
		o['bnum']=round2(arr2[i]['bnum']['sum'],3)-round2(arr2[i]['rbnum']['sum'],3);
	
		arr3.push(o);
	}
	return arr3;
}
//按商品分组  tb_store_mon
function querySt_G(arr1){	
	var arr2=_AW.groupby(arr1,'gclass,gname','samout,camout,profit,num,snum,bnum,rsnum,rbnum');
	var arr3=[];
	for(var i=0;i<arr2.length;i++){
		var o ={};		
		o['gclass']=arr2[i]['gclass'];
		o['gname']=arr2[i]['gname']	;
		o['samout']=round2(arr2[i]['samout']['sum'],3);		
		o['camout']=0-round2(arr2[i]['camout']['sum'],3);	
		o['profit']=round2(arr2[i]['profit']['sum'],3);	
		o['num']=round2(arr2[i]['num']['sum'],3);		
		o['snum']=round2(arr2[i]['rsnum']['sum'],3)-round2(arr2[i]['snum']['sum'],3);		
		o['bnum']=round2(arr2[i]['bnum']['sum'],3)-round2(arr2[i]['rbnum']['sum'],3);
	
		arr3.push(o);
	}
	//console.log(JSON.stringify(arr01));	
	return arr3;
}
//PKH:客户消费分布 -谁多谁少

function getBar_st_m(arr) {	
	
	var arrsamout=[];
	var arrcamout=[];
	var arrnum=[];
	var arrsnum=[];
	var arrbnum=[];
	var arrprofit=[];
	var legends=['销售额','采购额','利润','销售量','采购量','当月库存'];
	var weis=[];
	var arr2=[];
	console.log("arr"+JSON.stringify(arr))
	var arr0=querySt_M(arr);
	if (arr0==null||arr0.length==0)
		arr0=randomarray('monnum',['201801','201802','201803','201804','201805','201806','201807','201808','201809'],'samout,profit,num,snum')
	console.log("arr0"+JSON.stringify(arr0))
	
	for (var i=0;i<arr0.length;i++){
		arrsamout.push(arr0[i]['samout']);
		arrcamout.push(arr0[i]['camout']);
		arrprofit.push(arr0[i]['profit']);
		arrnum.push(arr0[i]['num']);
		arrsnum.push(arr0[i]['snum']);
		arrbnum.push(arr0[i]['bnum']);
		weis.push(arr0[i]['monnum']);		
	}	

	
	var barwidth=8;
	var arr2=[{
				name: '销售额',
				type: 'bar',
				barWidth: barwidth,
				'yAxisIndex':1,
				data: arrsamout
	},{
				name: '采购额',
				type: 'bar',
				barWidth: barwidth,
				'yAxisIndex':1,
				data: arrcamout
	},{
				name: '利润',
				type: 'bar',
				barWidth: barwidth,
				'yAxisIndex':1,
				data: arrprofit
	},{
				name: '销售量',
				type: 'bar',
				barWidth: barwidth,
				data: arrsnum
	},{
				name: '采购量',
				type: 'bar',
				barWidth: barwidth,
				data: arrbnum
	},{
				name: '当月库存',
				type: 'bar',
				barWidth: barwidth,		
				
				data: arrnum
	}];
	
	var option={
		color: _BARTHEME[0],
		toolbox : cssbarbox,
		legend: {
			data: legends,
			itemWidth: barwidth,
			x:'right',
		  	y:'30'
		},
		grid: cssgrid,
		dataZoom: cssdataZoom,
		yAxis: [{
			type: 'value',	
			silent: false,
			
		},{
			type: 'value',	
			silent: false,
			
		}],
		xAxis: [{
			data: weis,
			type: 'category',
			//nameLocation: 'start', // 坐标轴名字位置，支持'start' | 'end'
			scale: true,
			name: '',		
		}],
		series: arr2
	}

	return {
		title:charttitle.bstm,
		option:option
	}
}
function getPie_st_g(arr0) {	
	var arr01=_AW.groupby(arr0,'gclass','num');
	var arr1=[];
	var arr2=[];
	for (var i=0;i<arr01.length;i++){
		var o={};
		o['name']=arr01[i]['gclass']
		o['value']=arr01[i]['num']['sum']
		arr1.push(o);
		
	}	
	for (var i=0;i<arr0.length;i++){	
		var o={};
		o['name']=arr0[i]['gname']
		o['value']=arr0[i]['num']
		arr2.push(o);
	}
	arr0=null;
	arr01=null;
    arr1=[
	 {name:'面膜',value:'2020'},
	 {name:'保湿水',value:'20122'},
	 {name:'口红',value:'2222'}
	];
	 arr2=[
	 {name:'小红面膜',value:'1020'},
	 {name:'小月面膜',value:'1000'},
	 {name:'学习型保湿水',value:'522'},
	 {name:'小玩保湿水',value:'13022'},
	 {name:'小陈保湿水',value:'7012'},
	 {name:'小李口红',value:'2222'}
	];
	var arr3=_AW.getColList(arr1, 'name').concat(_AW.getColList(arr2, 'name'));	
	var md = {
		对象: arr3,
		组数据: arr1,
		支数据: arr2
	};
	var style = {
		normal: {
			label: {
				position: 'inner',
				formatter: function(params) {
					return(params.percent - 0).toFixed(0) + '%'
				}
			}
		},
		emphasis: {
			label: {
				show: true,
				formatter: "{b}\n{d}%"
			}
		}

	};
	var option = {
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b}: {c} ({d}%)"
		},
		legend: {
			data: md.对象,
			x:'right',
		  	y:'24'
		},
		toolbox : csspiebox,
		series: [{
				//name:'访问来源',
				type: 'pie',
				selectedMode: 'single',
				radius: [0, '30%'],
				itemStyle: style,
				data: md.组数据
			},
			{
				//name:'访问来源',
				type: 'pie',
				radius: ['38%', '55%'],
				data: md.支数据
			}
		]
	};
	return {
		title:charttitle.pstg,
		option:option
	}
}
 
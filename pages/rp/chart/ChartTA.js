//BTA

//分组统计  主表 tb_tradeacct
function queryTA_M(ta){
	var arr2= _AW.groupby(ta,'monnum','samout,camout,profit,income,expend');
	var arr3=[];
	for(var i=0;i<arr2.length;i++){
		var o ={};
		o['monnum']=arr2[i]['monnum'];		
		o['samout']=round2(arr2[i]['samout']['sum'],3);	
		o['camout']=round2(arr2[i]['camout']['sum'],3);	
		o['profit']=round2(arr2[i]['profit']['sum'],3);					
		o['income']=round2(arr2[i]['income']['sum'],3);	
		o['expend']=round2(arr2[i]['expend']['sum'],3);		
		arr3.push(o);
	}
	return arr3;
}


//分组统计 月 销售情况  主表 tb_tradeacct
function queryXS_M(ta){	
	var arr2= _AW.groupby(ta,'monnum','samout,profit,income');
	var arr3=[];
	for(var i=0;i<arr2.length;i++){
		var o ={};	
		o['monnum']=arr2[i]['monnum'];		
		o['samout']=round2(arr2[i]['samout']['sum'],3);		
		o['profit']=round2(arr2[i]['profit']['sum'],3);				
		o['income']=round2(arr2[i]['income']['sum'],3);				
		arr3.push(o);
	}
	return arr3;
}
//分组统计 用户销售情况  主表 tb_tradeacct
function queryXS_U(ta){	
	var arr2=_AW.groupby(ta,'cclass,uname','samout,profit,income');
	var arr3=[];
	for(var i=0;i<arr2.length;i++){
		var o ={};
		o['cclass']=arr2[i]['cclass'];
		o['uname']=arr2[i]['uname'];		
		o['samout']=round2(arr2[i]['samout']['sum'],3);		
		o['profit']=round2(arr2[i]['profit']['sum'],3);				
		o['income']=round2(arr2[i]['income']['sum'],3);				
		arr3.push(o);
	}
	return arr3;
}

	//收支利润总图
function getBar_ta_m(arr1) {	
	//测试数据
	if (arr1==null||arr1.length==0)
		arr1=randomarray('monnum',['201801','201802','201803','201804','201805','201806','201807','201808','201809'],'camout,samout,profit,income,expend')
	console.log(JSON.stringify(arr1))
	var md = {
		周期: _AW.getColList(arr1, 'monnum', function(e) {
			var v = Number(e.substr(4, 2));
			if(v % 2 == 0) return '\n' + v;
			return v + '月'
		}),
		采购: _AW.getColList(arr1, 'camout', function(e) {
			return 0 - e
		}),
		销售: _AW.getColList(arr1, 'samout'),
		利润: _AW.getColList(arr1, 'profit'),
		收入: _AW.getColList(arr1, 'income'),
		支出: _AW.getColList(arr1, 'expend', function(e) {
			return 0 - e
		}),
	};

	var option = {
		color: _BARTHEME[5],
		legend: {
			data: ['采购', '销售', '利润', '收入', '支出'],
			itemWidth: cssbarwidth,
			x:'right',
		  	y:'24'
		},
		toolbox : cssbarbox,
		grid: cssgrid,
		dataZoom: cssdataZoom,
		calculable: true,
		xAxis: {
			type: 'category',
			'axisLabel': {
				'interval': 0
			},
			data: md.周期,
			splitLine: {
				show: false
			},
			//nameLocation: 'end', // 坐标轴名字位置，支持'start' | 'end'
			scale: true,
			//name: '月份',
		},
		yAxis: [{
			type: 'value',
			nameLocation: 'end', // 坐标轴名字位置，支持'start' | 'end'
			scale: true,
			name: '（元）',

		}],
		series: [{
				name: '采购',
				type: 'bar',
				barWidth: cssbarwidth,
				itemStyle:cssbaritemStyle,
				stack: 'one',
				data: md.采购,
			},
			{
				name: '销售',
				type: 'bar',
				barWidth: cssbarwidth,
				itemStyle:cssbaritemStyle,
				stack: 'one',
				data: md.销售
			}, {
				name: '支出',
				type: 'bar',
				barWidth: cssbarwidth,
				itemStyle:cssbaritemStyle,
				stack: 'two',
				data: md.支出
			},
			{
				name: '收入',
				type: 'bar',
				barWidth: cssbarwidth,
				itemStyle:cssbaritemStyle,
				stack: 'two',
				data: md.收入
			}, {
				name: '利润',
				type: 'line',
				barWidth: cssbarwidth,
				data: md.利润,
				//normal: { label : {show: true, position: 'top'}},
				itemStyle:{normal: { label : {show: true, position: 'inside'}}},
				markLine: {
					data: [{
						type: 'average',
						name: '平均值'
					}]
				},
				
				areaStyle: {
					normal: {}
				},
			}
		]
	};
	return {
		title: charttitle.btam,
		option: option
	}

}
//销售-月-趋势柱图
function getBar_xs_m(arr) {	
	var arr0=queryXS_M(arr);	
	var legends=['销售额','收入','利润']
		//测试数据
	if (arr0==null||arr0.length==0)
		arr0=randomarray('monnum',['201801','201802','201803','201804','201805','201806','201807','201808','201809'],'samout,profit,income')
	//console.log(JSON.stringify(arr0))
	var arrsamout=[];
	var arrprofit=[];
	var arrincome=[];
	var weis=[];
	var arr2=[];
	for (var i=0;i<arr0.length;i++){
		arrsamout.push(arr0[i]['samout']);
		arrprofit.push(arr0[i]['profit']);
		arrincome.push(arr0[i]['income']);
		weis.push(arr0[i]['monnum']);		
	}	

	var arr2=[{
				name: '销售额',
				type: 'bar',
				barWidth: cssbarwidth,
				itemStyle:cssbaritemStyle,
				data: arrsamout,
				
	},{
				name: '收入',
				type: 'bar',
				barWidth: cssbarwidth,		
				itemStyle:cssbaritemStyle,
				data: arrincome,
				
	},{
				name: '利润',
				type: 'bar',
				barWidth: cssbarwidth,
				itemStyle:cssbaritemStyle,
				data: arrprofit
	}];
	
	var option={
		color: _BARTHEME[0],
		legend: {
			data: legends,
			itemWidth: cssbarwidth,
			x:'right',
		  	y:'24'
		},
		toolbox:cssbarbox,
		grid: cssgrid,
		dataZoom: cssdataZoom,
		yAxis: {
			type: 'value',	
			silent: false,
			splitLine: {
				show: false
			}
		},
		xAxis: [{
			data: weis,
			type: 'category',
			nameLocation: 'start', // 坐标轴名字位置，支持'start' | 'end'
			scale: true,
			name: '年月',		
		}],
		series: arr2
	}
	
	return {
		title:charttitle.bxsm,
		option:option
	}
}


//PKH:客户消费分布 -谁多谁少
function getPie_xs_u(arr0) {	
	var arr2=[];
	var arr1=[];
    if(arr0){
		var arr01=_AW.groupby(arr0,'cclass','samout');	
		for (var i=0;i<arr01.length;i++){
			var o={};
			o['name']=arr01[i]['cclass']
			o['value']=arr01[i]['samout']['sum']
			arr1.push(o);
		}	
		for (var i=0;i<arr0.length;i++){	
			var o={};
			o['name']=arr0[i]['uname']
			o['value']=arr0[i]['samout']
			arr2.push(o);
		}
		arr0=null;
		arr01=null;
	}
	console.log(JSON.stringify(arr1));
	console.log(JSON.stringify(arr2));
	if(arr1.length==0){
	    arr1=[
		 {name:'vip',value:'202'},
		 {name:'高级',value:'20122'},
		 {name:'中级',value:'82222'}
		];
	}
	if(arr2.length==0){
		arr2=[
		 {name:'小红',value:'202'},
		 {name:'小月',value:'20122'},
		 {name:'小刚',value:'522'},
		 {name:'小玩',value:'3022'},
		 {name:'小陈',value:'4012'},
		 {name:'小李',value:'502'}
		];
	}
	var arr3=_AW.getColList(arr1, 'name').concat(_AW.getColList(arr2, 'name'));	
	var md = {
		对象: arr3,
		组数据: arr1,
		支数据: arr2
	};
	var style = {
		 
		normal: {
			label: {
				position: 'inner',
				formatter: function(params) {
					return(params.percent - 0).toFixed(0) + '%'
				}
			}
		},
		emphasis: {
			label: {
				show: true,
				formatter: "{b}\n{d}%"
			}
		}

	};
	var option = {
//		title : {
//      text: '客户份额图',
//      x:'center'
//  },
   		toolbox : csspiebox,
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b}: {c} ({d}%)"
		},
		legend: {
			//orient: 'vertical',
        	//left: 'top',
        	x:'right',
		  	y:'20',
			data: md.对象
		},
		series: [{
				//name:'访问来源',
				type: 'pie',
				selectedMode: 'single',
				radius: [0, '30%'],
				itemStyle: style,
				data: md.组数据
			},
			{
				//name:'访问来源',
				type: 'pie',
				radius: ['38%', '55%'],
					itemStyle: style,
				data: md.支数据
			}
		]
	};
	return {
		title:charttitle.pxsu,
		option:option
	}
	
}
 
//BKH:客户消费排名-谁多谁少
/*
 * [{uname:'小红',samout:89.002},
 * {uname:'小明',samout:89.002},
 * {uname:'小女',samout:89.002},
 * {uname:'小车',samout:89.002},
 * {uname:'小d',samout:89.002},
 * {uname:'小a',samout:89.002}]
 */
function getBar_xs_u(arr) {	
	var arr0=queryXS_U(arr);
	//test
	// console.log(JSON.stringify(arr0));
	if (arr0.length==0){
		arr0=[{uname:'小红',samout:89.002},
	  {uname:'小明',samout:132.002},
	  {uname:'小女',samout:122.002},
	  
	  {uname:'小车',samout:23.002},
	  {uname:'小d',samout:1244.002},
	  {uname:'小a',samout:12.002}]
	}
	var arr2=[];
	var users=[];

	for (var i=0;i<arr0.length;i++){	
		users.push(arr0[i]['uname']);
		arr2.push(arr0[i]['samout']);
	}

	var option={
		color: _BARTHEME[0],
		legend: {
			data: ['销量'],
			itemWidth:10,
		  	x:'right',
		  	y:'24'
		},
		toolbox:cssbarbox,
		grid: cssgrid,
		dataZoom: cssdataZoom,
		xAxis: {
			type: 'category',
			data: users,
			silent: false,
			splitLine: {
				show: false
			}
		},
		yAxis: [{
			type: 'value',
			nameLocation: 'start', // 坐标轴名字位置，支持'start' | 'end'
			scale: true,
			name: '（元）',		
		}],
		series:{
				name: '销量',
				type: 'bar',
				barWidth: 15,	
				//itemStyle: {normal: {label : {show: true}}},
				itemStyle:cssbaritemStyle,
				data: arr2
		}
	}
	
	return {
		title:charttitle.bxsu,
		option:option
	}
}
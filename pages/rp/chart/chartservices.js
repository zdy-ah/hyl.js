

function randomarray(wei,data,model){
	var arr=[]
	for (var i=0;i<data.length;i++){
		var a={};
		a[wei]=data[i];
		md=model.split(',')
		for(var m in md){
			a[md[m]]=parseInt(1000*Math.random());			
		}
		arr.push(a);
	}
	return arr;
}
var _BARTHEME = [
	['#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#6495ed', '#40e0d0'],
	['#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0', '#da70d6'],
	['#1e90ff', '#ff6347', '#7b68ee', '#00fa9a', '#ffd700', '#32cd32'],
	['#6699FF', '#ff6666', '#3cb371', '#b8860b', '#30e0e0', '#ba55d3'],
	['#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#ffa500', '#ff6666'],
	['#6699FF', '#ff6666', '#3cb371', '#b8860b', '#ff69b4', '#cd5c5c'],
];

var csspiebox ={
                'show':true,            
                x: 'left', 
                y: 'top',
                'feature':{
                    'mark':{'show':true}, 
                    'magicType':{},
                    'restore':{'show':true},                   
                }
           };
var cssbarbox ={
                'show':true,            
                x: 'left', 
                y: 'top',
                'feature':{
                    'mark':{'show':true},                   
                    'magicType':{'show':true,'type':['line','bar','stack','tiled']},
                    'restore':{'show':true},                   
                }
            }
var cssdataZoom= {
			show: true,
			start: 0,
			end:100
		}
var cssgrid= {
			left: '4',
			right: '5%',
			top: '15%',
			bottom: '3%',
			containLabel: true
		}
var barWidth=15;
var cssbarwidth = 5;
var cssbaritemStyle= {normal: { label : {show: true, position: 'top'}}};
var charttitle={
	btam:'往来购销情况',
	bxsu:'客户消费排名',	
	bxsm:'客户消费规律',
	pxsu:'客户消费占比',
	bstm:'商品销售规律',
	pstg:'商品销售占比',	
	bmam:'总的收支情况',
}
//console.log(JSON.stringify(arr1))  ;

//查询明细 主表 tb_tradeacct
function queryTa(tacctwhere,contwhere) {
	var _tb_tradeacct = new XCDBHelp('tb_tradeacct');
	var contacts = new XCDBHelp('tb_contacts');
	var temp1 = new XCDBHelp('temp');
	var cont = contacts.select('uname,_idkey:cid2,cclass,clevel,pysx:py,cgroup:cg').getList();
	//console.log(JSON.stringify(cont));
	var arr = _tb_tradeacct.where(tacctwhere).getList();
	  
	var arr1 = _AW.leftjoin(arr, cont, 'cid=cid2');	
	//console.log(JSON.stringify(arr1));
	var where ={'cg': contwhere.cg,'cclass':contwhere.cclass,'clevel':contwhere.clevel,'uname%':contwhere.uname,'py%':contwhere.py}
 

	var arr2 = temp1.where(where).reduces(arr1);
	var arr3 = _AW.sort(arr2, 'monnum%d');
	return arr3;
}

//明细查询 tb_store_mon
function querySt(storewhere,goodwhere){	
	var _tb_store_mon = new XCDBHelp('tb_store_mon');
	var _tb_goods = new XCDBHelp('tb_goods');	
	var arr = _tb_store_mon.where(storewhere).getList();
	var ctarr =_tb_goods.where(goodwhere).select('_idkey:cid2,gclass,pysx:py,gname').getList();		
		
	//console.log("ctarr:"+JSON.stringify(arr))
	var arr2= _AW.leftjoin(arr,ctarr,'gid=cid2');
	
	var arr3 = _AW.sort(arr2, 'monnum%d');
	//console.log("arr3:"+JSON.stringify(arr3))	
	return arr3;
}

//明细查询 tb_myacct
function queryMa(where){
	var _tb_myacct = new XCDBHelp('tb_myacct');
	var arr = _tb_myacct.where(where).getList();
	return _AW.sort(arr,'monnum%d');	
}

//明细查询 tb_price_mon
function queryPr(where){
	var _tb_price_mon = new XCDBHelp('tb_price_mon');
	var _tb_goods = new XCDBHelp('tb_goods');		
	var arr = _tb_price_mon.where(where).getList();
	var ctarr =_tb_goods.select('_idkey:cid2,gclass,gname').getList();	
	return _AW.leftjoin(arr,ctarr,'gid=cid2');	
}

/////////////////////////////////


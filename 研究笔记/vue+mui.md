vue 与mui 混合时会出现很多奇怪的问题
例如mui组件不能使用
  解决步骤1
  在vue 的mounted中 初始化mui对象实例
  mounted:function(){
  		(function($, doc) {
  				$.init();
  				$.ready(function() {});
  		})(mui, document);
  }
  解决步骤2 把mui组件封装到vue组件中,目的
	Vue.component('mui-switch', {
				template: '<div class="mui-switch" :class="muiactive" @click="clickChange"><div class="mui-switch-handle"></div></div>',
				props: ['ext'],
				data: function() {
					return {
						st: true
					}
				},
				computed: {
					muiactive: function() {
						//console.log("muiactive")
						return {
							'mui-active': this.st
						}
					}
				},
				methods: {
					clickChange: function() {
						this.st = !this.st;
						this.$emit('switchChange', this.st);
					}
				}
			});
			
	
  这两种方法只能二选一 ,思路分析:
	1/mui,vue 实例化的先后顺序导致
	先初始化mui,如果再调用vue实例化,必然改变mui.init好的组件,导致init()失效
	若先调用vue实例化 ,再调用mui实例化,则没有这个问题
	所以用方法2可以解决大部分此类问题.
	2/为什么封装成组件可以呢?
	因为事件被覆盖掉了,原来switch组件应该自带一个toggle事件,
	但是被vue 渲染以后,switch组件被实例化成新的dom组件,所有原事件都失效了.
	
 vue的生命周期主
 
beforecreate-->created-->beforeMount-->Mounted 通常只进行一次
Mounted 如果有二次绑定,扩展元素绑定,那么还会调用
beforeupdate->updated 进行多次

- created :vue实例创建完后被调用
>，此时已经完成了数据观测(data observer)
>，属性和方法的运算
>，watch/event 事件回调的配置。

>可调用methods中的方法，访问和修改data中的数据
>并触发响应式变化使DOM渲染更新
>，触发watch中相应的方法
>，computed相关属性进行重新计算。
>一般在created时，进行ajax请求初始化实例数据。
- created --> beforeMount 之间
>在这个过程中，
>a、首先判断实例中是否有el选项，有的话继续向下编译，没有的话会停止编译，直到vm.$mount(el)被调用时才继续（el是挂载的DOM节点）；
>b、接下来判断实例中是否有template，有的话将其作为模板编译成rander函数；
>c、没有template的话就，就将 挂载DOM元素的html（即el所对应的DOM元素及其内部的元素）提取出来作为模板编译；
>*注：el所对应的DOM元素不能为body/html元素，因为在后面vue实例挂载时，el所对应的DOM元素及其内部的元素会被模板渲染出来的新的DOM所替换。
>d、如果实例对象中有rander函数，就直接通过它进行渲染操作。
>优先级：rander函数 > template > 外部html
>此时，rander函数已经准备好并首次被调用。
>在这个过程中，$el被初始化为实例中el选项所对应的DOM元素，所以在beforeMount时，用vm.$el获取到的是挂载DOM元素的html。
- Mounted
>此时实例已挂载到DOM上，可以通过DOM API获取实例中DOM节点。
>在控制台打印vm.$el，发现之前的挂载点及内容已被替换成新的DOM。

